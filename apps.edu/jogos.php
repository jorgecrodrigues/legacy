<?php
	require_once('jcms/setup/autoload.php');
	
	$jogo = new Jogo();
	JogosDAO::setObject($jogo);
	JogosDAO::DBConnection();
	JogosDAO::addFilter(array('ativa', '=', 1));
	JogosDAO::addSorter(array('nome', 'ASC'));
	JogosDAO::listItems(0,0);
	
?>
<!doctype html>
<html lang="pt-br">
	<head>
		<meta charset="ISO-8859-1">
		<title>Game In Class</title>
		<link href="/apps.edu/css/jquery-ui.css" rel="stylesheet">
		<style>
			body {
				font-size: 13px;
				font-family: verdana;
			}
			
			div#page {
				width: 710px;
				height: 100%;
				margin: auto;
			}
			
			header {
				margin-bottom: 80px;
			}
			
			header p {
				text-align: center;
			}
			
			header h1 {
				text-align: center;
			}
			
			footer {
			}
			
			footer p {
				text-align: center;
			}
			
			footer p span {
				cursor: pointer;
				color: #0c00ff;
				text-decoration: underline;
			}
			
			section {
				margin-bottom: 80px;
			}
			
			section p {
				text-align: center;
			}
			
			section div#container {
				margin-top: 40px;
				margin-bottom: 60px;
				width: 100%;
			}
			
			section div#container a {
				color: #000;
			}
			
		</style>
		<script src="/apps.edu/js/jquery-2.1.3.min.js"></script>
		<script src="/apps.edu/js/jquery-ui.min.js"></script>
		<script>
			$(
				function() {
					$("#dialog-creditos-lavi").dialog({
						autoOpen: false,
						resizable: false,
						draggable: false,
						height: 300,
						width: 510,						
						modal: true,
					});
				}
			);
			
			function openDialog() {
				$("#dialog-creditos-lavi").dialog( "open" );
			}
			
			function toggleAcesso() {
				$("div#container-acesso").toggle("slow");
			}
		</script>		
	</head>
	<body>
		<div id="page">
			<header>
				<p>
					<img src="/apps.edu/images/gic_logo.png" />
				</p>
				<h1>Lista dos Jogos Ativos</h1>
			</header>
			<section>
				<div id="container">
					<h2>Selecione um jogo para ver o ranking:</h2>
					<ul id="lista-jogos">
						<?php
							$rowIndex = 0;
							$numRows = $jogo->getNumRows();
							while ($rowIndex < $numRows) {
								JogosDAO::fillObject();
								?>
								<li><a href="/apps.edu/ranking/<?= $jogo->getSigla() ?>"><?= $jogo->getNome()." - ".$jogo->getSigla() ?></a></li>
								<?php
								$rowIndex++;
							}						
						?>
					</ul>
				</div>
			</section>
			<footer>
				<p>
					Game in Class 2015 – Desenvolvido por <span onclick="javascript:openDialog();">LAVI</span>  – Instituto de Computação – UFMT
					<br />
					Contato: gamificacao@ic.ufmt.br
				</p>
			</footer>
		</div>
		<div id="dialog-creditos-lavi" title="Créditos">
			<p style="text-align: center">
				<img src="/apps.edu/images/lavi_logo.png" />
			</p>
			<p>
				Laboratório de Ambientes Virtuais Interativos
				<br />
				Equipe: <strong>Prof. Jivago Medeiros</strong>, <strong>Profa. Karen Figueiredo</strong>
				 e <strong>Prof. Cristiano Maciel</strong>
				<br />
				Logo e artes por <strong>Jean Carlos Oliveira Santos</strong>
			</p>
		</div>
	</body>
</html>
