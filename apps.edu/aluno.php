<?php
require_once('jcms/setup/autoload.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"> 
        <meta charset="ISO-8859-1">
        <title>Gamifica��o - Profa. Karen</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <?php
            $alias = $_REQUEST['alias'];

            //retorna do banco de dados todos os jogos que um aluno com aquela alias est� cadastrado
            $jogoAluno = new Jogo();
            JogosDAO::setObject($jogoAluno);
            JogosDAO::DBConnection();
            JogosDAO::listByAliasAluno($alias);

            if ($jogoAluno->getNumRows() > 0) {
                ?>
                <h4>
                    <i class="glyphicon"><img src="http://gameinclass.ic.ufmt.br/apps.edu/jcms/view/images/chess.png" /></i>
                    &nbsp;<?= $alias ?>
                </h4>
				<ul id="myTab" class="nav nav-tabs">
					<?php
					$arr_jogos = array();
					$arr_alunos = array();
					$arr_pontos = array();
					$arr_total_pontos = array();

					$rowIndex = 0;
					$numRows = $jogoAluno->getNumRows();
					while ($rowIndex < $numRows) {
						JogosDAO::fillObject();
						$arr_jogos[$jogoAluno->getJogoID()] = clone $jogoAluno;
						$arr_pontos[$jogoAluno->getJogoID()] = $jogoAluno->getPtsSemanas();
						$arr_total_pontos[$jogoAluno->getJogoID()] = array_sum($arr_pontos[$jogoAluno->getJogoID()]);

						$aluno = new Aluno();
						$aluno->setAlias($alias);
						AlunosDAO::setObject($aluno);
						AlunosDAO::clearFilter();
						AlunosDAO::addFilter(array("alias", "=", $alias));
						AlunosDAO::addFilter(array("jogo_id", "=", $jogoAluno->getJogoID()));
						AlunosDAO::listItems(0, 0);
						AlunosDAO::fillObject();
						$arr_alunos[$jogoAluno->getJogoID()] = clone $aluno;
						?>
						<li <?= ($rowIndex == 0 ? "class=\"active\"" : null) ?>><a data-toggle="tab" href="#<?= $jogoAluno->getSigla() ?>"><?= $jogoAluno->getNome() ?></a></li>
						<?php
						$rowIndex++;
					}
					?>
				</ul>
                <div class="tab-content">
                    <?php
                        $index = 0;
                        foreach ($arr_jogos as $k => $jogo) {
                            $aluno = $arr_alunos[$k];
                    ?>
                    <div id="<?= $jogo->getSigla() ?>" class="tab-pane <?= ($index == 0 ? "active" : null) ?>">
						<?php
							if ($jogo->getAvisos()) {
						?>
						<br />
						<div class="alert alert-info">
							<?= $jogo->getAvisos() ?>
						</div>
						<?php
							}
						?>								
						<div class="panel-heading"><h4>Pontua��o das <?= $jogo->getLabelEtapas() ?></h4></div>
						<div class="panel panel-default">
							<div class="panel-body">
								<h1>Total de pontos <span class="label label-warning"><?= $aluno->getTotalPontos() . " / " . $arr_total_pontos[$k] ?></span></h1>
							</div>
						</div>
						<table class="table table-striped">
							<thead>
								<tr>
									<th><?= $jogo->getLabelEtapas() ?></th>
									<th>Pontua��o</th>
								</tr>
							</thead>
							<tbody>
							<?php
								$sem = "";
								$pts_aluno = 0;
						
								for ($i = 1; $i <= 15; $i++) {
									if ($arr_pontos[$k][$i]) {
										$pts_turma = $arr_pontos[$k][$i];
						
										$sem = "getPtsSem" . $i;
										$pts_aluno = $aluno->$sem();
						
										$perc = 0;
						
										if (($pts_turma > 0) && ($pts_aluno > 0)) {
											$perc = round(($pts_aluno / $pts_turma) * 100, 2);
										}
						
										$perc = ($perc > 100) ? 100 : $perc;
						
										$bar_class = "info";
						
										if ($perc >= 100) {
											$bar_class = "success";
										} else if ($perc < 50) {
											$bar_class = "warning";
										}
							?>
								<tr>
									<td><?= $jogo->getLabelEtapas() ?> <?= $i ?></td>
									<td>
										<div class="panel-body">
											<div class="progress" style="float: left; width: 90% !important">
												<div style="width: <?= $perc ?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?= $perc ?>" role="progressbar" class="progress-bar progress-bar-<?= $bar_class ?>">
													<span class="sr-only">&nbsp;</span>
												</div>
											</div>
											<div style="float: left; margin-left: 5px;"><small><?= $pts_aluno . "/" . $pts_turma ?></small></div>
										</div>                                                
									</td>
								</tr>
							<?php
									}
								}
							 ?>
							</tbody>
                        </table>                        
                        <div class="panel-heading"><h4>Medalhas</h4></div>
                        <div class="panel-body">
                            <div style="clear: both; margin-bottom: 5px;">
                                <div class="row">
									<?php
										$alunoMedalhas = new AlunoMedalhas($aluno->getAlunoID());
										AlunosMedalhasDAO::setObject($alunoMedalhas);
										AlunosMedalhasDAO::getObjectDBData();
										$arr_medalhas = $alunoMedalhas->getMedalhasID();
								
										foreach ($arr_medalhas as $m) {
											$medalha = new Medalha($m);
											MedalhasDAO::setObject($medalha);
											MedalhasDAO::getObjectDBData();
									?>
									<div class="col-xs-6 col-md-3" style="text-align: center">
                                        <div class="thumbnail">
											<?php
												if ($medalha->getImagem() && is_file("imagens/medalhas/" . $medalha->getImagem())) {
											?>
											<img src="/apps.edu/imagens/medalhas/<?= $medalha->getImagem() ?>" title="<?= $medalha->getNome() ?>" />
                                            <br />
                                            <strong><?= $medalha->getNome() ?></strong>
											<?php
												}
												else {
													echo $medalha->getNome();
												}
											?>
                                        </div>
                                    </div>
                                                            <?php
                                                        }
                                                        ?>
                                </div>
							</div>
							<div class="panel panel-default">
								<div class="panel-body">
									<h1>Total de medalhas <span class="label label-warning"><?= count($arr_medalhas) ?></span></h1>
								</div>
							</div>
                        </div>
                    </div>
				<?php
							$index++;
						}
				?>
            </div>                
			<?php
			}
			else {
			?>
			<div class="alert alert-danger">
				<h2>O ALUNO N�O EST� CADASTRADO EM NENHUM JOGO!</h2>
			</div>
			<?php
			}
			?>
        </div>
		<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script type='text/javascript' src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
		<script type='text/javascript'>
			$(document).ready(function () {
			});
		</script>
	</body>
</html>