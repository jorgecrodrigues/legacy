<?php
    
    require_once("../setup/autoload.php");
    define("INDEX",1);
    require('../setup/auth.php');    
    if (!$authUser) die("Para acessar essa p�gina � necess�rio efetuar login.");
    
    ob_start();
    
?>

<html>
    <head>
        <title>:: Gamifica��o - Profa. Karen Figueiredo ::</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <link type="text/css" href="css/geral.css" rel="stylesheet" />
        <link type="text/css" href="css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/colorbox.css" />
        <script src="js/geral.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script src="js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="js/jquery.maskedinput.js"></script>
    </head>

    <body>    
        <div id="header">
            <img src="images/barragerencia-mini.png" />
        </div>
        <div id="main">    
            <div id="left-side">
                <?php require_once("menu.php"); ?>
            </div>    
            <div id="right-side">							
                <?php
                    require("content/".$content_file);
                ?>
            </div>
        </div>
    </body>

</html>
<?php
    ob_flush();
?>