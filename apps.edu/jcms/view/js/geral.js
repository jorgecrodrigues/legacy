function deleteRecord(urlDel) {
    if (confirm("tem certeza que deseja excluir esse registro?"))
        window.location.href=urlDel;
    else
        alert("Registro n�o excluido"); 
}

function addFieldFoto() {
    var tbody_fotos = $("#tbody-fotos");
    tbody_fotos.append("<tr>"
                        +"<td><input type=\"file\" name=\"foto-file[]\" /> <input type=\"button\" value=\"excluir\" onclick=\"removeFieldFoto(this);\" /></td>"
                        +"</tr>");    
}

function removeFieldFoto(obj) {
    var td = obj.parentNode;
    $(td).remove();    
}

function addNewOption() {
    var tbody_opcoes = $("#tb-opcoes-enquete");
    
    tbody_opcoes.append("<tr>"
                        +"<td><input type=\"text\" name=\"opcoes[]\" size=\"60\" /></td>"
                        +"<td><input type=\"button\" value=\"excluir\" onclick=\"removeOption(this);\"></td>"
                        +"</tr>");
}

function removeOption(obj) {
    var td = obj.parentNode.parentNode;
    $(td).remove();    
}

function getViewport() {
    return [$(window).width(), $(window).height(), $(document).scrollLeft(), $(document).scrollTop() ];
};

function submitForm(form) {
    
    $.blockUI (
        {
            message:  "<p>Enviando dados. Seja paciente!</p>",
            css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                borderRadius: '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            }
        }
    );     
    
    $("#"+form).submit();
    
}

function selSelect(selID, selVal) {
    
    if (selVal != "") {
        $('#'+selID+' option').each(
            function() {
                if ($(this).val() == selVal) {
                    $(this).attr("selected","selected");
                    return false;
                }
                return 0;
            }
        );
    }
}

function addImagem() {
    $("div#galeria-conteudo").append("<p><input type=\"file\" name=\"imagens[]\" /><input type=\"button\" value=\"remover\" onclick=\"removeImagem(this);\" /></p>");
}

function removeImagem(obj) {
    var obj_p = obj.parentNode;
    $(obj_p).remove();    
}

function calculaPontuacao() {
    total=0;
    $('input.pts-semana').each(function() {
            if ($(this).val()!="") {
                total+=parseInt($(this).val());
            }
        }
    );
    return total;
}