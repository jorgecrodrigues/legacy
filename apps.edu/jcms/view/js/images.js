function addImage (url) {

	var urlText = "[foto="+url+"]";
	myField = document.getElementById('conteudo');
	//IE support
	if (document.selection) {
		myField.focus();
		sel = document.selection.createRange();
		sel.text = urlText;
	}
	//MOZILLA/NETSCAPE support
	else if (myField.selectionStart || myField.selectionStart == '0') {
		var startPos = myField.selectionStart;
		var endPos = myField.selectionEnd;
		myField.value = myField.value.substring(0, startPos)+urlText+myField.value.substring(endPos, myField.value.length);
	}
	else {
		myField.value += urlText;
	}
}

function addNewField() {
        var num_rows = document.getElementById('num_images');
        var objID = parseInt(num_rows.value,10)+1;
	var obj = document.getElementById('tb-add-fotos');
	var stdTR = obj.getElementsByTagName('tr')[1];
	var newTR = stdTR.cloneNode(true);
	newTR.style.display='block';
        newTR.getElementsByTagName('input')[0].value=objID;
        newTR.getElementsByTagName('input')[1].name="image-file-"+objID;
        newTR.getElementsByTagName('input')[2].name="exclusiva-img-"+objID;
	newTR.getElementsByTagName('input')[3].onclick=function() { removeField(this); };
        newTR.getElementsByTagName('textarea')[0].name="desc-pt-img-"+objID;
        newTR.getElementsByTagName('textarea')[1].name="desc-en-img-"+objID;
        newTR.getElementsByTagName('select')[0].name="fotografo-img-"+objID;        
	obj.getElementsByTagName('tbody')[1].appendChild(newTR);
        num_rows.value=objID;
        makeStripes();
}

function removeField(field) {
	var obj = document.getElementById('tb-add-fotos');
	var content = obj.getElementsByTagName('tbody')[1];	
	var node = field.parentNode.parentNode.parentNode;
	content.removeChild(node);
	node.style.display="none";
}

function makeStripes() {
    var obj = document.getElementById('tb-add-fotos');
    var content = obj.getElementsByTagName('tbody')[1];
    var listTR = content.getElementsByTagName('tr');
    var numTRs = listTR.length;
    
    for (i=1;i<numTRs;i++) {
        if (i%2==0) {
            listTR[i].getElementsByTagName('td')[0].style.backgroundColor = '#CCC';
        }
    }
    
    if (numTRs > 2) {
        var fotografo = listTR[numTRs-2].getElementsByTagName('select')[0].selectedIndex;
        listTR[numTRs-1].getElementsByTagName('select')[0].selectedIndex = fotografo;
    }
}