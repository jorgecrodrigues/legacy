function addEquipe(tabela_id, lista_id) {
    equipe = $("#"+lista_id+" option:selected");
    
    existe=false;
    
    if (equipe.val()!="0") {
        
        $("input[name^='equipe']").each(
            function() {
                if ($(this).val()==equipe.val()) {
                    existe=true;
                    alert(equipe.text()+" j� est� na lista!");
                    return false;   
                }
            }
        );
        
        if (!existe) {        
            tabela = $("#"+tabela_id);
            
            if ($(tabela).find('.none').size() > 0)
                $(tabela).find('.none').remove();
            
            tabela.append("<tr>"
                            +"<td>"
                            +"<input type=\"hidden\" name=\"equipe[]\" value=\""+equipe.val()+"\" >"+equipe.text()+"</td>"
                            +"<td><img src=\"images/delete_small.png\" onClick=\"javascript:removeEquipe(this);\" /></td>"
                          +"</tr>");
            
            $(tabela).tablesorter({sortList: [[0,0]] }); 
            
        }
        
        //$("#"+tabela_id+" tr:nth-child(odd)").addClass("odd");
        
        
    }
}

function removeEquipe(obj) {    
    
    table = $(obj).parent().parent().parent();
    
    $(obj).parent().parent().remove('tr');
    
    if ($(table).find('tr').size()==0) {
        $(table).append("<tr class=\"none\">"
                        +"<td colspan=\"2\">- nenhuma equipe associado ao campeonato -</td>"
                    +"</tr>");
    }
    
    
}