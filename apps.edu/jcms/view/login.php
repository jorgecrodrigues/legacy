<?php
    require_once("../setup/autoload.php");
?>
<html>
    <head>
        <title>Sistema para Gamifica��o de Cursos</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <style type="text/css">
        
            body {
                background: transparent url('images/bg_header.png') repeat-x scroll left top;
                font-family: verdana;
                font-size: 13px;
                border: none;
                margin: 0px;
                padding: 0px;
            }
            
            fieldset {
                border: 1px solid #999;
                border-radius: 4px;
                width: 450px;
            }
            
            fieldset a:link {
                color: #000;
                text-decoration: none;
            }
            
            fieldset a:visited, fieldset a:hover {
                color: #000;
                text-decoration: underline;        
            }
            
            legend {
                background: #015196;
                background-image: url('images/h3blue.gif');
                background-repeat: repeat-x;
                color: #FFF;
                font-weight: bold;
                padding: 7px;
            }
            
            div#main {
                left: 50%;
                margin-left: -300px;
                margin-top: 50px;
                position: absolute;
                width: 600px;
				max-width: 600px;
                
            }
            
            div#content {
                border: 1px solid #CCC;
                padding-top: 30px;
				width: 600px;
				max-width: 600px;
            }
            
            div#painel-login {
                margin-left: 100px;
            }
            
            div#footer {
                background-color: #CCC;
                margin-top: 60px;
                padding: 8px 0px 8px 6px;
                text-align: center;
                width: 594px;
            }
            
            .button {
                background-image: url('images/bg_button.png') !important;
                background-repeat: repeat-x;
                background-position: bottom center;
                border: 1px solid #CCC;
                color: #363636;
                font-size: 14px;
                height: 32px !important;
            }
            
            input.form-field {
                border: 1px solid #CCC;
                border-radius: 4px;
                color: #363636;
                font-size: 15px;
                height: 30px;
            }

            .msgError, .msgOk {
                color: #FFF;
                font-weight: bold;
                padding: 10px;
                width: 96%;
            }
            
            .msgError {
                background-color: #A13838;
            }
            
            .msgOk {
                background-color: #38A156;
            }
            
        </style>
    </head>
    <body>   
        <div id="main">
            <div id="content">
                <div id="header" style="text-align: center; width: 100%;">
                    <img src="images/gic_logo.png" />
                    <h3 style="background: transparent url('images/bg_bar_login.png') scroll repeat-x center bottom; padding-bottom: 6px; padding-top: 6px;">
						Sistema de Gamifica��o
					</h3>
                </div>
                <div id="painel-login" style="clear: both;">        
                    <form name="frm-login-cms" method="POST" action="<?= DIR_SYS."/core/controller/controller.php?face=backend&object=usuario&action=login" ?>">
                        <input type="hidden" name="<?= Forms::setFormToken("frm-login-cms") ?>" value="1" />
                        <fieldset>
                            <legend>Acessar sistema</legend>
                            <div style="clear: both">
                            <?php
                                if (isset($_SESSION['output_message'])) {
                                    print($_SESSION['output_message']);
                                    unset($_SESSION['output_message']);
                                }
                            ?>
                            </div>
                            <img src="images/cadeado.png" style="float: left;" />
                            <table>
                                <tr>
                                    <td><label for="login">Login:</label>&nbsp;</td>
                                    <td><input type="text" class="form-field" name="login" id="login" size="30" /></td>
                                </tr>
                                <tr>
                                    <td><label for="senha">Senha:</label>&nbsp;</td>
                                    <td><input type="password" class="form-field" id="senha" name="senha" size="30" /></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><input type="submit" class="button" value="Entrar" /></td>
                                </tr>
                            </table>
                        </fieldset>                
                    </form>
                </div>
                <div id="footer">
                    <p>
						� extremamente recomend�vel o uso dos navegadores <a href="http://br.mozdev.org/download/" target="_blank">Mozilla Firefox</a> 27.x+
						ou <a href="http://www.google.com.br/chrome/browser/desktop/" target="_blank">Google Chrome</a> 25.x+
					</p>
                </div>
            </div>
        </div>
    </body>
</html>