<?php
    
    require_once("../addons/fckeditor/fckeditor.php");

    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;

    $institucional = new Institucional($id);	
    InstitucionaisDAO::setObject($institucional);
    InstitucionaisDAO::DBConnection();
    InstitucionaisDAO::getObjectDBData();  
    
    Forms::setFormName("frm-edit-institucional");
    Forms::getFormData();
    $titulo = Forms::fieldValue("titulo",$institucional->getTitulo());
    $subTitulo = Forms::fieldValue("sub-titulo",$institucional->getSubTitulo());
    $chamada = Forms::fieldValue("chamada",$institucional->getChamada());
    $conteudo = Forms::fieldValue("conteudo",$institucional->getConteudo());
    Forms::unsetFormData();
    
?>

<h1>Formul�rio para atualizar Conte�do Institucional</h1>
<p class="cancel">
    <a href="lista-institucionais.php"><img src="images/voltar.png" title="voltar" alt="voltar" border="0" /></a>
</p>
<?php
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>
<form name="frm-edit-conteudo" method="POST" enctype="multipart/form-data" action="<?= DIR_SYS."/core/controller/controller.php?face=backend&object=institucional&action=atualizar&id=".$institucional->getConteudoID() ?>">               
<table class="tb-form" style="width: 100%;">
    <tbody>
        <tr>
            <th width="100">T�tulo *</th>
            <td><input type="text" id="titulo" class="form-field" name="titulo" size="40" value="<?= $titulo ?>" /></td>
        </tr>
        <tr>
            <th>Sub-t�tulo</th>
            <td><textarea type="text" id="sub-titulo" class="form-field" name="sub-titulo" cols="40" rows="2"><?= $subTitulo ?></textarea></td>
        </tr>        
        <tr>
            <th>Resumo</th>
            <td>
                <?php
                    $oFCKeditor = new FCKeditor("chamada") ;
                    $oFCKeditor->BasePath = "../addons/fckeditor/";
                    $oFCKeditor->Height = 250;
                    $oFCKeditor->Value = $chamada;
                    $oFCKeditor->Create() ;
                ?>
            </td>
        </tr>                             
        <tr>
            <th>Conte�do *</th>
            <td>
                <?php
                    $oFCKeditor = new FCKeditor("conteudo") ;
                    $oFCKeditor->BasePath = "../addons/fckeditor/";
                    $oFCKeditor->Height = 450;
                    $oFCKeditor->Value = $conteudo;
                    $oFCKeditor->Create() ;
                ?>
            </td>
        </tr>
            <tr>
                <th>Imagem destaque</th>
                <td class="small-content">
                    <?php
                        if (($institucional->getImagemDestaque()) && (@is_file("../../imagens/institucionais/".$institucional->getImagemDestaque()))) {
                            print("<img src=\"../../imagens/institucionais/".$institucional->getImagemDestaque()."\" width=\"200\" /><br/>");
                        }
                    ?>
                    <input type="file" name="imagem-destaque" id="imagem-destaque" />
                    <input type="hidden" name="imagem-atual" value="<?= $institucional->getImagemDestaque() ?>" />
                    <br><input type="checkbox" name="sem-imagem" value="1" />&nbsp;Sem imagem
                </td>
            </tr>
        <tr>
            <td colspan="2">Campos marcados com * s�o de preenchimento obrigat�rio.</td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" class="button" value="Atualizar" /></td>
        </tr>                        
    </tbody>
</table>