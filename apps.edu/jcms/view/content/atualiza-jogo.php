<?php

	require_once("../addons/fckeditor/fckeditor.php");
    
    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;
    
    $jogo = new Jogo($id);	
    JogosDAO::setObject($jogo);
    JogosDAO::DBConnection();
    JogosDAO::getObjectDBData();     
    
    Forms::setFormName("frm-edit-jogo");
    Forms::getFormData();
	$nome = Forms::fieldValue("nome",$jogo->getNome());
	$sigla = Forms::fieldValue("sigla",$jogo->getSigla());
	$label_etapas = Forms::fieldValue("label-etapas",$jogo->getLabelEtapas());
	$ranking = $jogo->getRanking();
	if ($ranking) {
		$ranking_pts = (int) Forms::fieldValue("chk-rkg-pts",$ranking{0});
		$ranking_pts_grupo = (int) Forms::fieldValue("chk-rkg-pts-grupo",$ranking{1});
		$ranking_medals = (int) Forms::fieldValue("chk-rkg-medals",$ranking{2});
		$ranking_medals_grupo = (int) Forms::fieldValue("chk-rkg-medals-grupo",$ranking{3});
	}
    $avisos = Forms::fieldValue("avisos",$jogo->getAvisos());
	$pts_sem1 = Forms::fieldValue("pts_sem1",$jogo->getPtsSem1());
    $pts_sem2 = Forms::fieldValue("pts_sem2",$jogo->getPtsSem2());
    $pts_sem3 = Forms::fieldValue("pts_sem3",$jogo->getPtsSem3());
    $pts_sem4 = Forms::fieldValue("pts_sem4",$jogo->getPtsSem4());
    $pts_sem5 = Forms::fieldValue("pts_sem5",$jogo->getPtsSem5());
    $pts_sem6 = Forms::fieldValue("pts_sem6",$jogo->getPtsSem6());
    $pts_sem7 = Forms::fieldValue("pts_sem7",$jogo->getPtsSem7());
    $pts_sem8 = Forms::fieldValue("pts_sem8",$jogo->getPtsSem8());
    $pts_sem9 = Forms::fieldValue("pts_sem9",$jogo->getPtsSem9());
    $pts_sem10 = Forms::fieldValue("pts_sem10",$jogo->getPtsSem10());
    $pts_sem11 = Forms::fieldValue("pts_sem11",$jogo->getPtsSem11());
    $pts_sem12 = Forms::fieldValue("pts_sem12",$jogo->getPtsSem12());
    $pts_sem13 = Forms::fieldValue("pts_sem13",$jogo->getPtsSem13());
    $pts_sem14 = Forms::fieldValue("pts_sem14",$jogo->getPtsSem14());
    $pts_sem15 = Forms::fieldValue("pts_sem15",$jogo->getPtsSem15());
	$ativo = (int) Forms::fieldValue("ativo",$jogo->getAtiva());
    Forms::unsetFormData();
    
?>
<h1>Formul�rio para atualizar Jogo</h1>
<p class="cancel">
    <a href="lista-jogos.php"><img src="images/voltar.png" title="cancelar" alt="cancelar" border="0" /></a>
</p>
<?php
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>			
<form id="frm-edit-jogo" name="frm-edit-jogo" method="POST" action="<?= DIR_SYS."/core/controller/controller.php?face=backend&object=jogo&action=atualizar&id=".$jogo->getJogoID() ?>">
    <table class="tb-form">
        <tbody>          
            <tr>
                <th width="150px">Nome</th>
                <td><input type="text" id="nome" class="form-field"  name="nome" size="40" value="<?= $nome ?>" /></td>
            </tr>
            <tr>
                <th>Sigla</th>
                <td><input type="text" id="sigla" class="form-field"  name="sigla" size="20" value="<?= $sigla ?>" /></td>
            </tr>
            <tr>
                <th>Label Etapas</th>
                <td><input type="text" id="label-etapas" class="form-field"  name="label-etapas" size="30" value="<?= $label_etapas ?>" /></td>
            </tr>
            <tr>
                <th>Exibi��o do Ranking</th>
                <td>
					<input name="chk-rkg-pts" type="checkbox" value="1" <?= $ranking_pts==1 ? "checked" : null ?> />&nbsp;Ranking por <strong>pontos individual</strong>
					<br class="block" />
					<input name="chk-rkg-pts-grupo" type="checkbox" value="1" <?= $ranking_pts_grupo==1 ? "checked" : null ?> />&nbsp;Ranking por <strong>pontos</strong> por <strong>grupo</strong>
					<br class="block" />
					<input name="chk-rkg-medals" type="checkbox" value="1" <?= $ranking_medals==1 ? "checked" : null ?> />&nbsp;Ranking por <strong>medalhas individual</strong>
					<br class="block" />
					<input name="chk-rkg-medals-grupo" type="checkbox" value="1" <?= $ranking_medals_grupo==1 ? "checked" : null ?> />&nbsp;Ranking por <strong>medalhas</strong> por <strong>grupo</strong>
					<br class="block" />					
				</td>
            </tr>			
			<tr>
				<th>Avisos</th>
				<td>
					<?php
						$oFCKeditor = new FCKeditor("avisos") ;
						$oFCKeditor->BasePath = "../addons/fckeditor/";
						$oFCKeditor->Height = 350;
						$oFCKeditor->Value = $avisos;
						$oFCKeditor->Create() ;
					?>
				</td>
			</tr>			
            <?php
                for ($i=1;$i<=15;$i++) {
                    $semana="pts_sem".$i;
            ?>
            <tr>
                <th>Pontos <?= $i."� ".$label_etapas ?>
				</th>
                <td><input type="text" class="form-field pts-semana" name="pontos-semana[<?= $i ?>]" size="6" value="<?= $$semana ?>" /></td>
            </tr>            
            <?php
                }
            ?>
			<tr>
				<td colspan="2" class="small-content">
					<input type="checkbox" name="jogo-ativo" value="1" <?= $ativo ? "checked" : null ?> />&nbsp;Jogo Ativo
				</td>
			</tr>
            <tr>
                <td colspan="2"><input type="button" class="button" onclick="javascript:submitForm('frm-edit-jogo');" value="Atualizar" /></td>
            </tr>                        
        </tbody>
    </table>
</form>