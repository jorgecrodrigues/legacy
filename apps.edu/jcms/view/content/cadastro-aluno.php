<?php
    
    require_once("../addons/fckeditor/fckeditor.php");
    
    Forms::setFormName("frm-add-aluno");
    Forms::getFormData();
	$jogo_id = Forms::fieldValue("jogo",0);
    $nome = Forms::fieldValue("nome","");
    $alias = Forms::fieldValue("alias","");
    $grupo = Forms::fieldValue("grupo","");
    Forms::unsetFormData();
    
?>
<h1>Formul�rio para cadastrar Aluno</h1>
<p class="cancel">
    <a href="lista-alunos.php"><img src="images/voltar.png" title="cancelar" alt="cancelar" border="0" /></a>
</p>
<?php
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>			
<form id="frm-add-aluno" name="frm-add-aluno" method="POST" action="<?= DIR_SYS."/core/controller/controller.php?face=backend&object=aluno&action=cadastrar" ?>">
    <table class="tb-form">
        <tbody>          
            <tr>
                <th width="150px">Nome <span class="required">*</span></th>
                <td><input type="text" id="nome" class="form-field"  name="nome" size="40" value="<?= $nome ?>" /></td>
            </tr>
            <tr>
                <th>Nome de Jogador <span class="required">*</span></th>
                <td><input type="text" class="form-field" name="alias" size="15" value="<?= $alias ?>" /></td>
            </tr>
            <tr>
                <th>Jogo <span class="required">*</span></th>
                <td>
                    <select name="jogo" class="form-field">
                        <option value="0">- nenhuma -</option>
						<?php
						
							$jogo=new Jogo();
							JogosDAO::setObject($jogo);
							JogosDAO::addSorter(array("nome","ASC"));
							JogosDAO::DBConnection();
							JogosDAO::listItems(0,0);
							
							$rowIndex=0;
							$numRows=$jogo->getNumRows();
							
							while ($rowIndex<$numRows) {
								JogosDAO::fillObject();
								?>
							<option value="<?= $jogo->getJogoID() ?>" <?= ($jogo_id==$jogo->getJogoID()) ? "selected" : null ?>>
								<?= $jogo->getNome().($jogo->getSigla() ? " - ".$jogo->getSigla() : null) ?>
							</option>
								<?php
								$rowIndex++;
							}                    
						
						?>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Grupo</th>
                <td><input type="text" class="form-field" name="grupo" size="20" value="<?= $grupo ?>" /></td>
            </tr>            
            <tr style="padding-bottom: 5px; padding-top: 5px;">
                <td colspan="2">Campos marcados com <span class="required">*</span> s�o de preenchimento obrigat�rio.</td>
            </tr>
            <tr>
                <td colspan="2"><input type="button" class="button" onclick="javascript:submitForm('frm-add-aluno');" value="Cadastrar" /></td>
            </tr>                        
        </tbody>
    </table>
</form>