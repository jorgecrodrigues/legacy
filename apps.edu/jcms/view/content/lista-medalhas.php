<?php

	$lista_jogos=ListasUtil::listaJogos();

    $filtro_jogo = isset($_REQUEST['filtro-jogo']) ? $_REQUEST['filtro-jogo'] : "";
    
    $medalha = new Medalha();
    MedalhasDAO::setObject($medalha);
    
    MedalhasDAO::DBConnection();
    
    if (!empty($filtro_jogo)) {
        MedalhasDAO::addFilter(array('jogo_id','=',$filtro_jogo));
		MedalhasDAO::addSorter(array('ordem','ASC'));
    }
	else {
		MedalhasDAO::addSorter(array('nome','ASC'));
	}
    
    MedalhasDAO::listItems(0,0);
    
?>
<h1>Listagem das Medalhas Cadastradas</h1>
<p class="cancel">
    <a href="cadastro-medalha.php"><img src="images/novo.png" title="novo registro" alt="novo registro" border="0" /></a>
</p>            
<?php
    Forms::setFormName("frm-delete-medalha");
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>
<div id="switch-order-filter" style="clear:both; display: table; width: 100%">
    <form id="form-opcoes-exibicao" name="form-opcoes-exibicao" method="GET" action="<?= $_SERVER['PHP_SELF'] ?>">
        <fieldset class="legenda" style="width: 45%; float: left;">
            <legend>Filtrar por:</legend>
            <select name="filtro-jogo" class="form-field">
                <option value="0"    <?= $filtro_jogo=="" ? "selected" : null ?>>- jogo -</option>
				<?php
					foreach ($lista_jogos as $k=>$d) {
						?>
				<option value="<?= $k ?>" <?= $filtro_jogo==$k ? "selected" : null ?>><?= $d ?></option>
						<?php
					}
				?>
            </select>
            <br style="display: block; clear: both; margin: 2px; margin-top: 14px;" />
			<input type="button" class="button" onclick="javascript:submitForm('form-opcoes-exibicao');" value="Filtrar" />
        </fieldset>
        <br style="display: block; clear: both; margin: 2px;" />
    </form>
</div>
<table class="tb-lista">
    <thead>
        <tr>
			<th>&nbsp;</th>
            <th>Nome</th>
            <th>Ordem</th>
            <th>Jogo</th>
            <th width="40">&nbsp;</th>
            <th width="40">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if ($medalha->getNumRows() > 0) {
                $rowIndex=0;
                $numRows = $medalha->getNumRows();
                while ($rowIndex < $numRows) {
                    MedalhasDAO::fillObject();
        ?>
        <tr class="<?= ($rowIndex%2==0) ? "escuro" : "claro" ?>">
			<td>
				<?php
					if ($medalha->getImagem() && is_file("../../imagens/medalhas/".$medalha->getImagem())) {
				?>
				<img src="../../imagens/medalhas/<?= $medalha->getImagem() ?>" />
				<?php
					}
					else {
						echo "-";
					}
				?>
			</td>
            <td><?= $medalha->getNome() ?></td>
            <td><?= $medalha->getOrdem() ?></td>
            <td><?= isset($lista_jogos[$medalha->getJogoID()]) ? $lista_jogos[$medalha->getJogoID()] : "-" ?></td>
            <td><a href="atualiza-medalha.php?id=<?= $medalha->getMedalhaID() ?>"><img src="images/edit_small.png" alt="editar" title="editar" border="0" /></a</td>
            <td><a href="javascript:deleteRecord('<?= DIR_SYS ?>/core/controller/controller.php?face=backend&object=medalha&action=deletar&id=<?= $medalha->getMedalhaID() ?>');"><img src="images/delete_small.png" alt="deletar" title="deletar" border="0" /></a></td>
        </tr>
        <?php
                    $rowIndex++;	
                }
            }
            else
                print("<tr><td colspan='6'>- nenhum registro cadastrado -</td></tr>");
        ?>                  
    </tbody>
</table>