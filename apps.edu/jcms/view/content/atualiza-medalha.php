<?php

    require_once("../addons/fckeditor/fckeditor.php");
	
	$id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null; 
	
	$medalha = new Medalha($id);
	MedalhasDAO::setObject($medalha);
	MedalhasDAO::DBConnection();
	MedalhasDAO::getObjectDBData();
	
    Forms::setFormName("frm-edit-medalha");
    Forms::getFormData();
	$jogo_id = Forms::fieldValue("jogo",$medalha->getJogoID());
    $nome = Forms::fieldValue("nome",$medalha->getNome());
    $descricao = Forms::fieldValue("descricao",$medalha->getDescricao());
	$ordem = Forms::fieldValue("ordem",$medalha->getOrdem());
    Forms::unsetFormData();
    
?>
<script type="text/javascript">
    $(document).ready(
        function () {
            $(".mask-date-time").mask("99/99/9999 99:99");
        }
    );
</script>
<h1>Formul�rio para atualizar Medalha</h1>
<p class="cancel">
    <a href="lista-medalhas.php"><img src="images/voltar.png" title="voltar" alt="voltar" border="0" /></a>
</p>
<?php
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>
<form id="frm-edit-medalha"
	  name="frm-edit-medalha" method="POST"
	  enctype="multipart/form-data"
	  action="<?= DIR_SYS."/core/controller/controller.php?face=backend&object=medalha&action=atualizar&id=".$medalha->getMedalhaID() ?>">
    <table class="tb-form" style="width: 100%;">
        <tbody>
            <tr>
                <th width="150">Nome <span class="required">*</span></th>
                <td><input type="text" name="nome" id="nome" class="form-field" size="40" value="<?= $nome ?>" /></td>
            </tr>
            <tr>
                <th>Jogo <span class="required">*</span></th>
                <td>
                    <select name="jogo" class="form-field">
                        <option value="0">- nenhuma -</option>
						<?php
						
							$jogo=new Jogo();
							JogosDAO::setObject($jogo);
							JogosDAO::addSorter(array("nome","ASC"));
							JogosDAO::DBConnection();
							JogosDAO::listItems(0,0);
							
							$rowIndex=0;
							$numRows=$jogo->getNumRows();
							
							while ($rowIndex<$numRows) {
								JogosDAO::fillObject();
								?>
							<option value="<?= $jogo->getJogoID() ?>" <?= ($jogo_id==$jogo->getJogoID()) ? "selected" : null ?>>
								<?= $jogo->getNome().($jogo->getSigla() ? " - ".$jogo->getSigla() : null) ?>
							</option>
								<?php
								$rowIndex++;
							}                    
						
						?>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Descri��o</th>
                <td>
                    <?php
                        $oFCKeditor = new FCKeditor("descricao") ;
                        $oFCKeditor->BasePath = "../addons/fckeditor/";
                        $oFCKeditor->Height = 250;
                        $oFCKeditor->Value = $descricao;
                        $oFCKeditor->Create() ;
                    ?>
                </td>
            </tr>
            <tr>
                <th>Ordem</th>
                <td><input type="text" name="ordem" id="ordem" class="form-field" size="6" value="<?= $ordem ?>" /></td>
            </tr>
            <tr>
                <th>Imagem <span class="required">*</span></th>
                <td>
                    <?php

                        if ($medalha->getImagem() && is_file("../../imagens/medalhas/".$medalha->getImagem())) {
                            print("<img src=\"../../imagens/medalhas/".$medalha->getImagem()."\" /><br/>");
                        }
                    
                    ?>
                    <input type="file" name="imagem" id="imagem" />
                    <input type="hidden" name="imagem-atual" value="<?= $medalha->getImagem() ?>" />
                </td>
            </tr>
            <tr>
                <td colspan="2">Campos marcados com <span class="required">*</span> s�o de preenchimento obrigat�rio.</td>
            </tr>                            
            <tr>
                <td colspan="2"><input type="button" class="button" onclick="javascript:submitForm('frm-edit-medalha');" value="Atualizar" /></td>
            </tr>
        </tbody>
    </table>
</form>