<?php
    
    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;
    
    $aluno = new Aluno($id);	
    AlunosDAO::setObject($aluno);
    AlunosDAO::DBConnection();
    AlunosDAO::getObjectDBData();
	
    $alunoMedalhas = new AlunoMedalhas($aluno->getAlunoID());
    AlunosMedalhasDAO::setObject($alunoMedalhas);
    AlunosMedalhasDAO::getObjectDBData();
    
    Forms::setFormName("frm-edit-aluno");
    Forms::getFormData();
    $nome = Forms::fieldValue("nome",$aluno->getNome());
    $alias = Forms::fieldValue("alias",$aluno->getAlias());
    $grupo = Forms::fieldValue("grupo",$aluno->getGrupo());
    $jogo_id = Forms::fieldValue("jogo",$aluno->getJogoID());
    $pts_sem1 = Forms::fieldValue("pts_sem1",$aluno->getPtsSem1());
    $pts_sem2 = Forms::fieldValue("pts_sem2",$aluno->getPtsSem2());
    $pts_sem3 = Forms::fieldValue("pts_sem3",$aluno->getPtsSem3());
    $pts_sem4 = Forms::fieldValue("pts_sem4",$aluno->getPtsSem4());
    $pts_sem5 = Forms::fieldValue("pts_sem5",$aluno->getPtsSem5());
    $pts_sem6 = Forms::fieldValue("pts_sem6",$aluno->getPtsSem6());
    $pts_sem7 = Forms::fieldValue("pts_sem7",$aluno->getPtsSem7());
    $pts_sem8 = Forms::fieldValue("pts_sem8",$aluno->getPtsSem8());
    $pts_sem9 = Forms::fieldValue("pts_sem9",$aluno->getPtsSem9());
    $pts_sem10 = Forms::fieldValue("pts_sem10",$aluno->getPtsSem10());
    $pts_sem11 = Forms::fieldValue("pts_sem11",$aluno->getPtsSem11());
    $pts_sem12 = Forms::fieldValue("pts_sem12",$aluno->getPtsSem12());
    $pts_sem13 = Forms::fieldValue("pts_sem13",$aluno->getPtsSem13());
    $pts_sem14 = Forms::fieldValue("pts_sem14",$aluno->getPtsSem14());
    $pts_sem15 = Forms::fieldValue("pts_sem15",$aluno->getPtsSem15());
    $total_pontos = Forms::fieldValue("total-pontos",$aluno->getTotalPontos());
	$medalhas_keys = Forms::fieldValue("medalhas",$alunoMedalhas->getMedalhasID());
    Forms::unsetFormData();
	
	$objJogo = new Jogo($jogo_id);
	JogosDAO::setObject($objJogo);
	JogosDAO::DBConnection();
	JogosDAO::getObjectDBData();	
    
?>
<script>
    $(document).ready(
        function () {
            $("span#total-pontos").html(calculaPontuacao());
            $('input[name="total-pontos"]').val(calculaPontuacao());
            $('input.pts-semana').on("blur",function() {
                $("span#total-pontos").html(calculaPontuacao());
                $('input[name="total-pontos"]').val(calculaPontuacao());                
            });
        }
    );
</script>
<h1>Formul�rio para atualizar Aluno</h1>
<p class="cancel">
    <a href="lista-alunos.php"><img src="images/voltar.png" title="cancelar" alt="cancelar" border="0" /></a>
</p>
<?php
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>			
<form id="frm-edit-aluno" name="frm-edit-aluno" method="POST" action="<?= DIR_SYS."/core/controller/controller.php?face=backend&object=aluno&action=atualizar&id=".$aluno->getAlunoID() ?>">
    <table class="tb-form">
        <tbody>          
            <tr>
                <th width="150px">Nome <span class="required">*</span></th>
                <td><input type="text" id="nome" class="form-field"  name="nome" size="40" value="<?= $nome ?>" /></td>
            </tr>
            <tr>
                <th>Alias <span class="required">*</span></th>
                <td><input type="text" class="form-field" name="alias" size="15" value="<?= $alias ?>" /></td>
            </tr>
            <tr>
                <th>Jogo <span class="required">*</span></th>
                <td>
                    <select name="jogo" class="form-field">
                        <option value="0">- nenhum -</option>
						<?php
						
							$jogo=new Jogo();
							JogosDAO::setObject($jogo);
							JogosDAO::addSorter(array("nome","ASC"));
							JogosDAO::DBConnection();
							JogosDAO::listItems(0,0);
							
							$rowIndex=0;
							$numRows=$jogo->getNumRows();
							
							while ($rowIndex<$numRows) {
								JogosDAO::fillObject();
								?>
							<option value="<?= $jogo->getJogoID() ?>" <?= ($jogo_id==$jogo->getJogoID()) ? "selected" : null ?>>
								<?= $jogo->getNome().($jogo->getSigla() ? " - ".$jogo->getSigla() : null) ?>
							</option>
								<?php
								$rowIndex++;
							}                    
						
						?>
                    </select>
                </td>
            </tr>
            <tr>
                <th>Grupo</th>
                <td><input type="text" class="form-field" name="grupo" size="20" value="<?= $grupo ?>" /></td>
            </tr>            
            <tr>
                <th>Medalhas</th>
                <td>
					<div class="medalha-row">
					<?php
					
						$medalha = new Medalha();
						MedalhasDAO::setObject($medalha);						
						MedalhasDAO::DBConnection();
						MedalhasDAO::addFilter(array('jogo_id','=',$aluno->getJogoID()));
						MedalhasDAO::addSorter(array('ordem','ASC'));
						MedalhasDAO::listItems(0,0);
					
						if ($medalha->getNumRows() > 0) {
							$rowIndex=0;
							$numRows = $medalha->getNumRows();
							while ($rowIndex < $numRows) {
								MedalhasDAO::fillObject();		
					
					?>
					<div class="medalha-cell">
						<input type="checkbox" name="medalhas[]"
							   value="<?= $medalha->getMedalhaID() ?>"
							   <?= (array_search($medalha->getMedalhaID(), $medalhas_keys) !== false) ? "checked" : "null" ?>
							   />
						<br />
						<?php
							if ($medalha->getImagem() && is_file("../../imagens/medalhas/".$medalha->getImagem())) {
						?>
						<img src="../../imagens/medalhas/<?= $medalha->getImagem() ?>" title="<?= $medalha->getNome() ?>" />
						<?php
							}
							else {
								echo $medalha->getNome();
							}
						?>						
					</div>
					<?php
								$rowIndex++;
							}
						}
					?>
					</div>
                </td>
            </tr>
            <?php
                for ($i=1;$i<=15;$i++) {
                    $semana="pts_sem".$i;
            ?>
            <tr>
                <th>Pts <?= $i."� ".$objJogo->getLabelEtapas() ?> </th>
                <td><input type="text" class="form-field pts-semana" name="pontos-semana[<?= $i ?>]" size="6" value="<?= $$semana ?>" /></td>
            </tr>            
            <?php
                }
            ?>
            <tr>
                <th>Total de pontos:</th>
                <td>
                    <input type="hidden" name="total-pontos" value="<?= $total_pontos ?>" />
                    <span id="total-pontos"></span>
                </td>
            </tr>            
            <tr style="padding-bottom: 5px; padding-top: 5px;">
                <td colspan="2">Campos marcados com <span class="required">*</span> s�o de preenchimento obrigat�rio.</td>
            </tr>
            <tr>
                <td colspan="2"><input type="button" class="button" onclick="javascript:submitForm('frm-edit-aluno');" value="Atualizar" /></td>
            </tr>                        
        </tbody>
    </table>
</form>