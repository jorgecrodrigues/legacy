<?php

	$lista_jogos=ListasUtil::listaJogos();

    $filtro_jogo = isset($_REQUEST['filtro-jogo']) ? $_REQUEST['filtro-jogo'] : "";
    $filtro_grupo  = isset($_REQUEST['filtro-grupo']) ? $_REQUEST['filtro-grupo'] : "";

    $ordena = isset($_REQUEST['ordena']) ? (int) $_REQUEST['ordena'] : 1;
    
    $aluno = new Aluno();
    AlunosDAO::setObject($aluno);
    
    AlunosDAO::DBConnection();
    
    $array_grupos = AlunosDAO::getGrupos();
    
    if (!empty($filtro_jogo))
        AlunosDAO::addFilter(array('jogo_id','=',$filtro_jogo));

    if (!empty($filtro_grupo))
        AlunosDAO::addFilter(array('grupo','=',$filtro_grupo));
        
    if (!empty($ordena)) {
        switch ($ordena) {
            case 1 :
                AlunosDAO::addSorter(array("nome","ASC"));
                break;
            case 2 :
                AlunosDAO::addSorter(array("nome","DESC"));
                break;
            case 3 :
                AlunosDAO::addSorter(array("total_pontos","ASC"));
                break;
            case 4 :
                AlunosDAO::addSorter(array("total_pontos","DESC"));
                break;
            case 5 :
                AlunosDAO::addSorter(array("total_medalhas","ASC"));
                break;
            case 6 :
                AlunosDAO::addSorter(array("total_medalhas","DESC"));            
            default :
                AlunosDAO::addSorter(array("nome","ASC"));
        }
    }
    
    
    AlunosDAO::listItems(0,0);
    
?>
<h1>Listagem dos Alunos cadastrados</h1>
<p class="cancel">
    <a href="cadastro-aluno.php"><img src="images/novo.png" title="novo registro" alt="novo registro" border="0" /></a>
</p>            
<?php
    Forms::setFormName("frm-delete-aluno");
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>
<div id="switch-order-filter" style="clear:both; display: table; width: 100%">
    <form id="form-opcoes-exibicao" name="form-opcoes-exibicao" method="GET" action="<?= $_SERVER['PHP_SELF'] ?>">
        <fieldset class="legenda" style="width: 45%; float: left;">
            <legend>Filtrar por:</legend>
            <select name="filtro-jogo" class="form-field">
                <option value="0"    <?= $filtro_jogo=="" ? "selected" : null ?>>- jogo -</option>
				<?php
					foreach ($lista_jogos as $k=>$d) {
						?>
				<option value="<?= $k ?>" <?= $filtro_jogo==$k ? "selected" : null ?>><?= $d ?></option>
						<?php
					}
				?>
            </select>
            <br style="display: block; clear: both; margin: 2px;" />
            <select name="filtro-grupo" class="form-field">
                <option value="0"  <?= $filtro_grupo=="" ? "selected" : null ?>>- grupo -</option>
                <?php
                    if (is_array($array_grupos)) {
                        foreach ($array_grupos as $g) {
                    ?>
                <option value="<?= $g['grupo'] ?>" <?= $filtro_grupo==$g['grupo'] ? "selected" : null ?>><?= $g['grupo'] ?></option>
                    <?php
                        }
                    }
                ?>
            </select>            
        </fieldset>
        <fieldset class="legenda" style="width: 45%; float: left;">
            <legend>Ordenar por:</legend>
            <select name="ordena" class="form-field">
                <option value="1" <?= $ordena==1 ? "selected" : null ?>>Nome crescente</option>
                <option value="2" <?= $ordena==2 ? "selected" : null ?>>Nome decrescente</option>
                <option value="3" <?= $ordena==3 ? "selected" : null ?>>Pontua��o crescente</option>
                <option value="4" <?= $ordena==4 ? "selected" : null ?>>Pontua��o decrescente</option>
                <option value="5" <?= $ordena==5 ? "selected" : null ?>>Medalhas crescente</option>
                <option value="6" <?= $ordena==6 ? "selected" : null ?>>Medalhas decrescente</option>
            </select>
        </fieldset>
        <br style="display: block; clear: both; margin: 2px;" />
        <input type="button" class="button" onclick="javascript:submitForm('form-opcoes-exibicao');" value="Filtrar e Ordenar" />        
    </form>
</div>
<table class="tb-lista">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Nome de Jogador</th>
            <th>Jogo</th>
            <th>Grupo</th>
            <th width="40">&nbsp;</th>
            <th width="40">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if ($aluno->getNumRows() > 0) {
                $rowIndex=0;
                $numRows = $aluno->getNumRows();
                while ($rowIndex < $numRows) {
                    AlunosDAO::fillObject();
        ?>
        <tr class="<?= ($rowIndex%2==0) ? "escuro" : "claro" ?>">
            <td><?= $aluno->getNome() ?></td>
            <td><?= $aluno->getAlias() ?></td>
            <td><?= isset($lista_jogos[$aluno->getJogoID()]) ? $lista_jogos[$aluno->getJogoID()] : "-" ?></td>
            <td><?= $aluno->getGrupo() ?></td>
            <td><a href="atualiza-aluno.php?id=<?= $aluno->getAlunoID() ?>"><img src="images/edit_small.png" alt="editar" title="editar" border="0" /></a</td>
            <td><a href="javascript:deleteRecord('<?= DIR_SYS ?>/core/controller/controller.php?face=backend&object=aluno&action=deletar&id=<?= $aluno->getAlunoID() ?>');"><img src="images/delete_small.png" alt="deletar" title="deletar" border="0" /></a></td>
        </tr>
        <?php
                    $rowIndex++;	
                }
            }
            else
                print("<tr><td colspan='6'>- nenhum registro cadastrado -</td></tr>");
        ?>                  
    </tbody>
</table>