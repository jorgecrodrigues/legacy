<h1>P�gina Inicial</h1>			
<?php
    if (isset($_SESSION['output_message'])) {
        print($_SESSION['output_message']);
        unset($_SESSION['output_message']);
    }
?>
<p style="background-color: #F2F2F2; padding-bottom: 10px; padding-top: 10px; padding-left: 4px;">
    <img src="images/profile.png" />&nbsp;Ol� <strong><?= $usuario_ativo->getNomeCompleto() ?></strong>, Seja bem-vindo(a) ao Sistema <strong>Game In Class</strong>
</p>
<p style="background-color: #F2F2F2; padding-bottom: 10px; padding-top: 10px; padding-left: 4px;">
    Sistema desenvolvido em car�ter experimental para auxiliar no estudo da Gamifica��o na Educa��o em disciplinas de Ensino Superior.
</p>
<p>
    <br/><img src="images/exit.png" />&nbsp;<a href="<?= DIR_SYS."/core/controller/controller.php?face=backend&object=usuario&action=logoff" ?>">Sair</a>
</p>
<hr/>
<h2>Configura��es</h2>
<form name="frm-configs" method="POST" action="<?= DIR_SYS."/core/controller/controller.php?face=backend&object=configuracoes&action=salvar" ?>">
    <table class="tb-form">
        <?php
            $configs = array();

            $sorterBy = null;
            $filterBy = null;   

            $config  = new Configuracao();
            ConfiguracoesDAO::setObject($config);
            ConfiguracoesDAO::DBConnection();
            ConfiguracoesDAO::listItems($sorterBy, $filterBy, 0, 0);
            
            $rowIndex=0;
            $numRows=$config->getNumRows();
            
            while ($rowIndex < $numRows) {
                ConfiguracoesDAO::fillObject();
                $configs[$config->getName()] = $config->getValue();
                $rowIndex++;
            }
         
        ?>
        <tr>
            <th>
                Alterar a senha do sistema&nbsp;
            </th>
            <td>
                <table class="tb-form">
                    <tbody>
                        <tr>
                            <td>Nova senha</td>
                            <td><input type="password" class="form-field" size="30" name="senha" /></td>
                        </tr>
                        <tr>
                            <td>Redigite</td>
                            <td><input type="password" class="form-field" size="30" name="re-senha" /></td>
                        </tr>                                    
                    </tbody>
                </table>
            </td>
        </tr>        
        <tr>
            <td colspan="2"><input type="submit" class="button" value="Atualizar configura��es" /></td>
        </tr>
    </table>
</form>