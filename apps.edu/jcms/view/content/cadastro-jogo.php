<?php

	require_once("../addons/fckeditor/fckeditor.php");
    
    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;
    
    $jogo = new Jogo($id);	
    JogosDAO::setObject($jogo);
    JogosDAO::DBConnection();
    JogosDAO::getObjectDBData();     
    
    Forms::setFormName("frm-add-jogo");
    Forms::getFormData();
	$nome = Forms::fieldValue("nome","");
	$sigla = Forms::fieldValue("sigla","");
	$label_etapas = Forms::fieldValue("label-etapas","");
	$ranking_pts = (int) Forms::fieldValue("chk-rkg-pts","");
	$ranking_pts_grupo = (int) Forms::fieldValue("chk-rkg-pts-grupo","");
	$ranking_medals = (int) Forms::fieldValue("chk-rkg-medals","");
	$ranking_medals_grupo = (int) Forms::fieldValue("chk-rkg-medals-grupo","");
	$ativo = (int) Forms::fieldValue("ativo","");
    Forms::unsetFormData();
    
?>
<h1>Formul�rio para cadastrar Jogo de Disciplina</h1>
<p class="cancel">
    <a href="lista-jogos.php"><img src="images/voltar.png" title="cancelar" alt="cancelar" border="0" /></a>
</p>
<?php
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>			
<form id="frm-add-jogo" name="frm-add-jogo" method="POST" action="<?= DIR_SYS."/core/controller/controller.php?face=backend&object=jogo&action=cadastrar" ?>">
    <table class="tb-form">
        <tbody>          
            <tr>
                <th width="150px">Nome&nbsp;<span class="required">*</span></th>
                <td><input type="text" id="nome" class="form-field"  name="nome" size="40" value="<?= $nome ?>" /></td>
            </tr>
            <tr>
                <th>Sigla&nbsp;<span class="required">*</span></th>
                <td><input type="text" id="sigla" class="form-field"  name="sigla" size="20" value="<?= $sigla ?>" /></td>
            </tr>
            <tr>
                <th>Label Etapas&nbsp;<span class="required">*</span></th>
                <td><input type="text" id="label-etapas" class="form-field"  name="label-etapas" size="30" value="<?= $label_etapas ?>" /></td>
            </tr>
            <tr>
                <th>Exibi��o do Ranking</th>
                <td>
					<input name="chk-rkg-pts" type="checkbox" value="1" <?= $ranking_pts==1 ? "checked" : null ?> />&nbsp;Ranking por <strong>pontos individual</strong>
					<br class="block" />
					<input name="chk-rkg-pts-grupo" type="checkbox" value="1" <?= $ranking_pts_grupo==1 ? "checked" : null ?> />&nbsp;Ranking por <strong>pontos</strong> por <strong>grupo</strong>
					<br class="block" />
					<input name="chk-rkg-medals" type="checkbox" value="1" <?= $ranking_medals==1 ? "checked" : null ?> />&nbsp;Ranking por <strong>medalhas individual</strong>
					<br class="block" />
					<input name="chk-rkg-medals-grupo" type="checkbox" value="1" <?= $ranking_medals_grupo==1 ? "checked" : null ?> />&nbsp;Ranking por <strong>medalhas</strong> por <strong>grupo</strong>
					<br class="block" />					
				</td>
            </tr>
            <tr>
                <td colspan="2"><input type="button" class="button" onclick="javascript:submitForm('frm-add-jogo');" value="Cadastrar" /></td>
            </tr>                        
        </tbody>
    </table>
</form>