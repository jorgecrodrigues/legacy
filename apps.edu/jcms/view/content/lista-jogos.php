<?php
    
    $jogo = new Jogo();
    JogosDAO::setObject($jogo);
    JogosDAO::DBConnection();
	JogosDAO::addSorter(array('nome','ASC'));
    JogosDAO::listItems(0,0);
    
?>
<h1>Listagem dos Jogos Cadastrados</h1>
<p class="cancel">
    <a href="cadastro-jogo.php"><img src="images/novo.png" title="novo registro" alt="novo registro" border="0" /></a>
</p>
<?php
    Forms::setFormName("frm-delete-jogo");
    $outputMessage=Forms::getOutputMessage();
    if ($outputMessage) {
        echo $outputMessage;
        Forms::resetOutputMessage();
    }
?>
<table class="tb-lista">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Sigla</th>
			<th>Ativo</th>
            <th width="40">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if ($jogo->getNumRows() > 0) {
                $rowIndex=0;
                $numRows = $jogo->getNumRows();
                while ($rowIndex < $numRows) {
                    JogosDAO::fillObject();
        ?>
        <tr class="<?= ($rowIndex%2==0) ? "escuro" : "claro" ?>">
            <td><?= $jogo->getNome() ?></td>
            <td><?= $jogo->getSigla() ?></td>
			<td><?= $jogo->getAtiva() ? "<span class='sim'>sim</span>" : "<span class='nao'>n�o</span>" ?></td>
            <td><a href="atualiza-jogo.php?id=<?= $jogo->getJogoID() ?>"><img src="images/edit_small.png" alt="editar" title="editar" border="0" /></a</td>
        </tr>
        <?php
                    $rowIndex++;	
                }
            }
            else
                print("<tr><td colspan='3'>- nenhum registro cadastrado -</td></tr>");
        ?>                  
    </tbody>
</table>