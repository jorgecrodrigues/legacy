<?php

    $institucional = new Institucional();
    InstitucionaisDAO::DBConnection();
    InstitucionaisDAO::setObject($institucional);

    $sorterBy = array(array("conteudo_id","ASC"));
    $filterBy = null;
    
    InstitucionaisDAO::listItems($sorterBy,$filterBy,0,0);
    
?>

<h1>Listagem dos Conte�dos Institucionais</h1>
<p class="cancel">
    &nbsp;
</p>
<table class="tb-lista">
    <thead>
        <tr>
            <th>T�tulo</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
    <?php                    
        if ($institucional->getNumRows() > 0) {
            $rowIndex=0;
            $numRows = $institucional->getNumRows();
            while ($rowIndex < $numRows) {
                InstitucionaisDAO::fillObject();
                ?>
        <tr class="<?= ($rowIndex%2==0) ? "escuro" : "claro" ?>">
            <td><?= $institucional->getTitulo() ?></td>
            <td width="100px"><a href="atualiza-institucional.php?id=<?= $institucional->getConteudoID() ?>"><img src="images/edit_small.png" alt="editar" title="editar" border="0" /></a></td>
        </tr>
                <?php
                $rowIndex++;	
            }
        }
        else
            print("<tr><td colspan=\"2\">- nenhum registro cadastrado.</td></tr>");                    
    ?>                    
    </tbody>
</table>