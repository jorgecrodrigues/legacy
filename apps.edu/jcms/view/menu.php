<h3 style="background-color: #CCC; margin: 0px; padding: 2px 0px 2px 3px;">Menu</h3>
<ul id="left-menu">
    <li><img src="images/desktop.png" /> <a href="index.php"> Inicial</a></li>
    <hr/>
    <li><img src="images/disciplinas.png"  alt="" /> Jogos
        <ul>
            <li>- <a href="cadastro-jogo.php">Adicionar</a></li>
			<li>- <a href="lista-jogos.php">Listar</a></li>
        </ul>
    </li>
    <hr/>
    <li><img src="images/alunos.png"  alt="" /> Alunos
        <ul>
            <li>- <a href="cadastro-aluno.php">Adicionar</a></li>
            <li>- <a href="lista-alunos.php">Listar</a></li>
        </ul>
    </li>
    <hr/>
	<li><img src="images/medalhas.png"  alt="" /> Medalhas
        <ul>
            <li>- <a href="cadastro-medalha.php">Adicionar</a></li>
            <li>- <a href="lista-medalhas.php">Listar</a></li>
        </ul>
    </li>	
</ul>