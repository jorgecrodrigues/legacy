<?php

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");   
    
    Forms::setFormName("frm-add-imovel");
    
    $nome = $_REQUEST['nome'];
    $tipo_imovel = (int) $_REQUEST['imovel-tipo'];
    $categoria = (int) $_REQUEST['categoria'];
    
    $sucess=false;
    
    if (!empty($nome) && !empty($tipo_imovel) && !empty($categoria)) {
    
        $valor = $_REQUEST['valor-imovel'];
        if ($valor) {
            $valor = str_replace(".","",$_REQUEST['valor-imovel']);
            $valor = str_replace(",",".",$valor);
        }
    
        $imovel = new Imovel();
        $imovel->setImovelTipoID($tipo_imovel);
        $imovel->setCategoriaID($categoria);
        $imovel->setCodigo($_REQUEST['codigo']);
        $imovel->setNome($nome);
        $imovel->setValor($valor);
        $imovel->setQuartos($_REQUEST['quartos']);
        $imovel->setSuites($_REQUEST['suites']);
        $imovel->setResumo($_REQUEST['resumo']);
        $imovel->setCidade($_REQUEST['cidade']);
        $imovel->setUf($_REQUEST['estado']);
        $imovel->setBairro($_REQUEST['bairro']);
        $imovel->setDescricao($_REQUEST['descricao']);
        $imovel->setDestaque($_REQUEST['destaque'] ? 1 : 0);       
        
        ImoveisDAO::setObject($imovel);
        ImoveisDAO::DBConnection();
        
        if (ImoveisDAO::insertItem()) {
            
            File::createDir("../../../imagens/imoveis/imovel".$imovel->getLastInsertedID());
            
            if (isset($_FILES['foto-principal']) && is_array($_FILES['foto-principal']) && !empty($_FILES['foto-principal']['name']) && File::isImage($_FILES['foto-principal']['name'])) {
                $foto_imovel = "foto-principal".".".File::extension($_FILES['foto-principal']['name']);        
                $dirImovel = "imovel".$imovel->getLastInsertedID();
                $pathDirFotos = "../../../imagens/imoveis/".$dirImovel."/";
                File::put($_FILES['foto-principal']['tmp_name'], $pathDirFotos.$foto_imovel);
                $imagem_principal = new Image($foto_imovel,"../../../imagens/imoveis/".$dirImovel);
                $imagem_principal->resizeImage(1024,768,1,$foto_imovel,"../../../imagens/imoveis/".$dirImovel);
                $imagem_principal->resizeImage(170,90,0,"thumb-".$foto_imovel,"../../../imagens/imoveis/".$dirImovel);
                $imovel->setFotoPrincipal($foto_imovel);
                $imovel->setImovelID($imovel->getLastInsertedID());
                ImoveisDAO::atualizarFotoPrincipal();
            }
            
            $outputMessage="<p class='jcms-msg-ok'>Im�vel cadastrado com sucesso!</p>";
            $sucess=true;
        }
        else
            $outputMessage="<p class='jcms-msg-error'>N�o foi poss�vel cadastrar im�vel!<br/>Error: ".$imovel->getErrorMsg()."</p>";	
    
    }
    else
        $outputMessage="<p class='jcms-msg-error'>Im�vel n�o cadastrado!<br/>Campos marcados com * s�o obrigat�rios.</p>";		

    if (!$sucess)
        Forms::setFormData($_REQUEST);
        
    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);      


?>