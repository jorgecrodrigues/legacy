<?php

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");   
    
    Forms::setFormName("frm-edit-imovel");
    
    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;	
    $imovel = new Imovel($id);     
    
    $nome = $_REQUEST['nome'];
    $tipo_imovel = (int) $_REQUEST['imovel-tipo'];
    $categoria = (int) $_REQUEST['categoria'];
    
    $sucess=false;
    
    if (!empty($nome) && !empty($tipo_imovel) && !empty($categoria)) {
    
        $valor = $_REQUEST['valor-imovel'];
        if ($valor) {
            $valor = str_replace(".","",$_REQUEST['valor-imovel']);
            $valor = str_replace(",",".",$valor);
        }
    
        $imovel->setImovelTipoID($tipo_imovel);
        $imovel->setCategoriaID($categoria);
        $imovel->setCodigo($_REQUEST['codigo']);
        $imovel->setNome($nome);
        $imovel->setValor($valor);
        $imovel->setQuartos($_REQUEST['quartos']);
        $imovel->setSuites($_REQUEST['suites']);
        $imovel->setResumo($_REQUEST['resumo']);
        $imovel->setCidade($_REQUEST['cidade']);
        $imovel->setUf($_REQUEST['estado']);
        $imovel->setBairro($_REQUEST['bairro']);
        $imovel->setDescricao($_REQUEST['descricao']);
        $imovel->setDestaque($_REQUEST['destaque'] ? 1 : 0);       
        
        ImoveisDAO::setObject($imovel);
        ImoveisDAO::DBConnection();
        
        if (ImoveisDAO::updateItem()) {
            
            $dirImovel = "imovel".$imovel->getImovelID();
            $pathDirFotos = "../../../imagens/imoveis/".$dirImovel."/";            
            
            if (isset($_REQUEST['sem-imagem']) && ($_REQUEST['sem-imagem']==1)) {
                if (!empty($_REQUEST['foto-atual']) && is_file($pathDirFotos.$_REQUEST['foto-atual'])) {
                    File::remove($pathDirFotos.$_REQUEST['foto-atual']);
                }
                $imovel->setFotoPrincipal("");
            }
            else if (!empty($_FILES['foto-principal']['name'])) {
                $image = $_FILES['foto-principal'];
                if (File::isImage($image['name'])) {
                    $foto_name = "foto-principal".".".File::extension($_FILES['foto-principal']['name']);
                    
                    if (!empty($_REQUEST['foto-atual']) && $foto_name != $_REQUEST['foto-atual']) {
                        File::remove($pathDirFotos.$foto_name);
                        File::remove($pathDirFotos."thumb-".$foto_name);
                    }
                    
                    if (File::put($_FILES['foto-principal']['tmp_name'], $pathDirFotos.$foto_name)) {
                        $imagem_principal = new Image($foto_imovel,"../../../imagens/imoveis/".$dirProduto);
                        $imagem_principal->resizeImage(800,600,1,$foto_imovel,"../../../imagens/imoveis/".$dirProduto);
                        $imagem_principal->resizeImage(170,90,0,"thumb-".$foto_imovel,"../../../imagens/imoveis/".$dirImovel);
                        $imovel->setFotoPrincipal($foto_name);
                    }
                    
                }
            }
            else
                $imovel->setFotoPrincipal($_REQUEST['foto-atual']);
                
            ImoveisDAO::atualizarFotoPrincipal();
            
            $outputMessage="<p class='jcms-msg-ok'>Im�vel atualizado com sucesso!</p>";
            $sucess=true;
        }
        else
            $outputMessage="<p class='jcms-msg-error'>N�o foi poss�vel atualizar im�vel!<br/>Error: ".$imovel->getErrorMsg()."</p>";	
    
    }
    else
        $outputMessage="<p class='jcms-msg-error'>Im�vel n�o atualizado!<br/>Campos marcados com * s�o obrigat�rios.</p>";		

    if (!$sucess)
        Forms::setFormData($_REQUEST);
        
    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);      


?>