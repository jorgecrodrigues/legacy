<?php

    if (!defined("INDEX")) {
        die("Esse script n�o pode ser acessado diretamente!");
    }
    
    Forms::setFormName("frm-add-pics");
    
    $numFiles = sizeof($_FILES['foto-file']['name']);
    
    if ($numFiles > 0) {
    
        $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;
    
        if (!empty($id)) {
        
            $imovel = new Imovel($id);
            ImoveisDAO::setObject($imovel);
            ImoveisDAO::DBConnection();
            ImoveisDAO::getObjectDBData();
            
            if ($imovel->getNumRows() > 0) {
            
                $dirImovel = "imovel".$imovel->getImovelID();
                $pathDirFotos = "../../../imagens/imoveis/".$dirImovel."/";
        
                $errorFiles = "";
        
                for ($i=0;$i<$numFiles;$i++) {
                    if ( !empty($_FILES['foto-file']['name'][$i]) ) {
                        if (File::isImage($_FILES['foto-file']['name'][$i])) {
                            $newFileName = File::removeSpecialChars($_FILES['foto-file']['name'][$i]);
                            if (File::put($_FILES['foto-file']['tmp_name'][$i], $pathDirFotos.$newFileName)) {
                                $image = new Image($newFileName,"../../../imagens/imoveis/".$dirImovel);
                                $image->resizeImage(800,600,1,$newFileName);
                                $thumb = new Image($newFileName,"../../../imagens/imoveis/".$dirImovel);
                                $thumb->resizeImage(120,120,1,"thumb-".$newFileName);                                
                            }
                            else
                                $errorFiles .= "".$_FILES['foto-file']['name'][$i]." -> erro enviando este arquivo!</p>";
                        }
                        else
                            $errorFiles .= "".$_FILES['foto-file']['name'][$i]." -> n�o � um arquivo de imagem v�lido!</p>";
                    }
                }
        
                if (!empty($errorFiles))
                    $outputMessage = "<p class='jcms-msg-error'>".$errorFiles."</p>";
            }
            else
                $outputMessage = "<p class='jcms-msg-error'>Fotos n�o cadastradas! Im�vel n�o encontrado.</p>";
        }
        else
            $outputMessage = "<p class='jcms-msg-error'>Fotos n�o cadastradas! Im�vel inv�lido.</p>";        
    }
    
    $key = $id;
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);     
    
?>