<?php

    Forms::setFormName("frm-planilha-produtos");

    $outputMessage="";

    if (isset($_FILES['planilha-produtos'])) {
        
        $planilha = new Spreadsheet_Excel_Reader($_FILES['planilha-produtos']['tmp_name']);
        
        if (($planilha->rowcount() > 1) && ($planilha->colcount() == 6)) {
            
            $linhas = $planilha->rowcount();
            
            $inseridos=0;
            $atualizados=0;
            
            for ($i=2;$i<=$linhas;$i++) {
                
                $codigo_produto=trim($planilha->val($i,1));
                $fornecedor_nome=trim($planilha->val($i,2));
                $categoria_nome=trim($planilha->val($i,3));
                $nome_produto=trim($planilha->val($i,4));
                $resumo_produto=trim($planilha->val($i,5));
                $descricao_produto=trim($planilha->val($i,6));
                
                $produto = new Produto();
                $produto->setCodigo($codigo_produto);
                ProdutosDAO::setObject($produto);
                ProdutosDAO::DBConnection();
                ProdutosDAO::getObjectDBData();
                
                if ($produto->getNumRows() > 0) {
                    
                    if ($nome_produto)
                        $produto->setNome($nome_produto);

                    if ($resumo_produto)
                        $produto->setResumo($resumo_produto);
                    
                    if ($descricao_produto)
                        $produto->setDescricao($descricao_produto);
                        
                    if ($fornecedor_nome) {
                        $fornecedor=new Fornecedor();
                        $fornecedor->setNome($fornecedor_nome);
                        FornecedoresDAO::setObject($fornecedor);
                        FornecedoresDAO::getObjectDBData();
                        if ($fornecedor->getNumRows() > 0)
                            $produto->setFornecedorID($fornecedor->getFornecedorID());
                        else
                            $produto->setFornecedorID(0);
                    }
                    
                    if ($categoria_nome) {
                        $categoria=new Categoria();
                        $categoria->setNome($categoria_nome);
                        CategoriasDAO::setObject($categoria);
                        CategoriasDAO::getObjectDBData();
                        if ($categoria->getNumRows() > 0)
                            $produto->setCategoriaID($categoria->getCategoriaID());
                        else
                            $produto->setCategoriaID(0);                                          
                    }
                    
                    if (ProdutosDAO::updateItem())
                        $atualizados++;
                    
                }
                else {
                    
                    if ($nome_produto)
                        $produto->setNome($nome_produto);
                        
                    if ($resumo_produto)
                        $produto->setResumo($resumo_produto);                        
                    
                    if ($descricao_produto)
                        $produto->setDescricao($descricao_produto);
                        
                    if ($fornecedor_nome) {
                        $fornecedor=new Fornecedor();
                        $fornecedor->setNome($fornecedor_nome);
                        FornecedoresDAO::setObject($fornecedor);
                        FornecedoresDAO::getObjectDBData();
                        if ($fornecedor->getNumRows() > 0)
                            $produto->setFornecedorID($fornecedor->getFornecedorID());
                        else
                            $produto->setFornecedorID(0);
                    }
                    
                    if ($categoria_nome) {
                        $categoria=new Categoria();
                        $categoria->setNome($categoria_nome);
                        CategoriasDAO::setObject($categoria);
                        CategoriasDAO::getObjectDBData();
                        if ($categoria->getNumRows() > 0)
                            $produto->setCategoriaID($categoria->getCategoriaID());
                        else
                            $produto->setCategoriaID(0);                                          
                    }
                    
                    if (ProdutosDAO::insertItem()) {
                        File::createDir(PATH_SITE."/imagens/produtos/produto".$produto->getLastInsertedID());
                        $inseridos++;
                    }
                    
                }
                $outputMessage="<p class='jcms-msg-ok'>Planilha de Produtos processada com sucesso! No total ".$inseridos." novos produtos foram inseridos e ".$atualizados." produtos atualizados</p>";   
            }
        }
        else
            $outputMessage="<p class='jcms-msg-error'>A planilha de Produtos enviada n�o � compat�vel com o padr�o esperado!</p>";
        
    }
    
    if (isset($_FILES['planilha-embalagens'])) {
        
        $planilha = new Spreadsheet_Excel_Reader($_FILES['planilha-embalagens']['tmp_name']);
        
        if (($planilha->rowcount() > 1) && ($planilha->colcount() == 5)) {
            
            $linhas = $planilha->rowcount();
            
            $registros=0;
            
            for ($i=2;$i<=$linhas;$i++) {
                
                $codigo_produto=trim($planilha->val($i,1));
                
                $produto = new Produto();
                $produto->setCodigo($codigo_produto);
                ProdutosDAO::setObject($produto);
                ProdutosDAO::DBConnection();
                ProdutosDAO::getObjectDBData();
                
                if ($produto->getNumRows() > 0) {
                    $embalagem_nome=trim($planilha->val($i,2));
                    $embalagem = new Embalagem();
                    $embalagem->setNome($embalagem_nome);
                    EmbalagensDAO::setObject($embalagem);
                    EmbalagensDAO::getObjectDBData();
                    
                    if ($embalagem->getNumRows() > 0) {
                        
                        $preco_base=trim($planilha->val($i,3));
                        $desconto_padrao=trim($planilha->val($i,4));
                        $disponivel=($planilha->val($i,5)=="SIM") ? 1 : 0;
                        
                        $produtoEmbalagens = new ProdutoEmbalagens();
                        $produtoEmbalagens->setProdutoID($produto->getProdutoID());
                        $produtoEmbalagens->setEmbalagens(array($embalagem->getEmbalagemID()));
                        $produtoEmbalagens->setPrecosBase(array($preco_base));
                        $produtoEmbalagens->setDescontosPadrao(array($desconto_padrao));
                        $produtoEmbalagens->setDisponiveis(array($disponivel));
                        
                        ProdutosEmbalagensDAO::setObject($produtoEmbalagens);
                        
                        if (ProdutosEmbalagensDAO::insertOrUpdateOne())
                            $registros++;
                        
                    }
                }
                
            }
            
            $outputMessage.="<p class='jcms-msg-ok'>Planilha de Embalagens \ Pre�os processada com sucesso! Total de ".$registros." inseridos ou atualizados</p>";
            
        }
        else
            $outputMessage.="<p class='jcms-msg-error'>A planilha de Embalagens \ Pre�os enviada n�o � compat�vel com o padr�o esperado!</p>";
    }
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);

?>