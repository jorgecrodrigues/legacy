<?php

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");
    
    $imovelID = (int) $_REQUEST['id'];
	
    $sucess=false;
        
    if (!empty($imovelID)) {		
        
        $imovel = new Imovel($imovelID);
        ImoveisDAO::setObject($imovel);        
        ImoveisDAO::DBConnection();
        ImoveisDAO::getObjectDBData();
        
        if (ImoveisDAO::deleteItem()) {
            
            $dirImovel = "imovel".$imovel->getImovelID();
            $pathDirFotos = "../../../imagens/imoveis/".$dirImovel."/";   
    
            //$dir = PATH_SYS."/view/imoveis/fotos/imovel".$imovel->getImovelID();
            if (is_dir($pathDirFotos)) {
                if ($dirLink = opendir($pathDirFotos)) {
                    while (($file = readdir($dirLink)) !== false) {
                        File::remove($pathDirFotos.$file);
                        File::remove($pathDirFotos."thumb-".$file);
                    }
                    closedir($dirLink);
                }
            }
            
            rmdir($pathDirFotos);
            
            $sucess=true;
            
        }
        else
            $outputMessage="<p class='jcms-msg-error'>Erro ao tentar excluir Im�vel.<br/>".$imovel->getErrorMsg()."</p>";                 
    }
    else
        $outputMessage="<p class='jcms-msg-error'>Im�vel n�o excluido!<br/>Registro n�o localizado.</p>"; 

    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage); 


?>