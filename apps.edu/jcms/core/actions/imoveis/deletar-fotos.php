<?php				

    if (!defined("INDEX")) {
        die("Esse script n�o pode ser acessado diretamente!");
    }
    
    Forms::setFormName("frm-del-pics");

    $fotos = $_REQUEST['foto'];
    $numFiles = sizeof($_REQUEST['foto']);

    if ($numFiles > 0) {

        $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;

        if (!empty($id)) {

            $imovel = new Imovel($id);
            ImoveisDAO::setObject($imovel);
            ImoveisDAO::DBConnection();
            ImoveisDAO::getObjectDBData();
            
            if ($imovel->getNumRows() > 0) {
            
                $errorFiles = "";    
            
                $dirImovel = "imovel".$imovel->getImovelID();
                $pathDirFotos = "../../../imagens/imoveis/".$dirImovel."/";            
            
                for ($i=0;$i<$numFiles;$i++) {
                    if ( !empty($fotos[$i]) ) {
                        if (File::isImage($fotos[$i])) {
                            $delFile = $pathDirFotos.$fotos[$i];
                            if (File::remove($delFile)) {
                                $delFile = $pathDirFotos."thumb-".$fotos[$i];
                                File::remove($delFile);
                            }
                        }
                    }
                }
                
            }
            else
                $outputMessage = "<p class='jcms-msg-error'>Fotos n�o cadastradas! Imovel n�o encontrado.</p>";            
        }
        else
            $outputMessage = "<p class='jcms-msg-error'>Fotos n�o deletadas! Imovel inv�lido.</p>";         
    }
    else
        $outputMessage="<p class='jcms-msg-error'>Fotos n�o deletadas! Nenhum arquivo selecionado.</p>";
    
    $key = $id;
    
    $outputMessage=$errorFiles;
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage); 
    
?>