<?php

    if (!defined("INDEX")) {
        die("Esse script n�o pode ser acessado diretamente!");
    }
    
    Forms::setFormName("frm-prod-embalagens");
    
    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;

    if (!empty($id)) {
    
        $produto = new Produto($id);
        ProdutosDAO::setObject($produto);
        ProdutosDAO::DBConnection();
        ProdutosDAO::getObjectDBData();
    
        if ($produto->getNumRows() > 0) {
            
            $embalagens = new ProdutoEmbalagens();
            $embalagens->setProdutoID($produto->getProdutoID());
            ProdutosEmbalagensDAO::setObject($embalagens);
            ProdutosEmbalagensDAO::deleteItem();
            
            if (isset($_REQUEST['embalagens']) && is_array($_REQUEST['embalagens']) && (sizeof($_REQUEST['embalagens']) > 0)) {
            
                $embalagensID = $_REQUEST['embalagens'];
            
                $embalagens->setEmbalagens($embalagensID);
                
                $precos_base = array();
                $descontos_padrao = array();
                $disponiveis = array();
                
                foreach ($embalagensID as $emb) {
                    if (isset($_REQUEST['preco-base-emb-'.$emb])) {
                        $preco = $_REQUEST['preco-base-emb-'.$emb];
                        $preco = str_replace(".","", $preco);
                        $preco = str_replace(",",".", $preco);                    
                    }
                    else
                        $preco=0.00;
                    array_push($precos_base, $preco);

                    if (isset($_REQUEST['desc-padrao-emb-'.$emb])) {
                        $desconto = $_REQUEST['desc-padrao-emb-'.$emb];
                    }
                    else
                        $desconto=0.00;
                    array_push($descontos_padrao, $desconto);
                    
                    $disponivel = empty($_REQUEST['emb-disp-'.$emb]) ? 0 : 1;
                    array_push($disponiveis, $disponivel);
                }
                
                $embalagens->setPrecosBase($precos_base);
                $embalagens->setDescontosPadrao($descontos_padrao);
                $embalagens->setDisponiveis($disponiveis);
                
                ProdutosEmbalagensDAO::insertItem();            
            }
        }
    
    }
    
    $key = $id;
    
    
?>