<?php


    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");
    
    $outputMessage="";
    
    if (isset($_REQUEST['senha']) && !empty($_REQUEST['senha'])) {
        
        if (isset($_REQUEST['re-senha']) && !empty($_REQUEST['re-senha'])) {
        
            if ($_REQUEST['senha'] === $_REQUEST['re-senha']) {
        
                $config = new Configuracao();
                $config->setName('PASSWORD_CMS');
                $config->setValue(md5($_REQUEST['senha']));
            
                ConfiguracoesDAO::setObject($config);
                ConfiguracoesDAO::DBConnection();
                
                if (ConfiguracoesDAO::updateItem())
                    $outputMessage.="<p class='jcms-msg-ok'>Senha do CMS alterada com sucesso!</p>";
                else
                    $outputMessage.="<p class='jcms-msg-error'>N�o foi poss�vel alterar a senha do CMS!<br/>Error: ".$config->getErrorMsg()."</p>";
            }
            else
                $outputMessage.="<p class='jcms-msg-error'>Senha n�o alterada!<br/>Senha n�o confere com a senha de confirma��o.</p>";
        }
        else
            $outputMessage.="<p class='jcms-msg-error'>Senha n�o alterada!<br/>redigite a senha no campo especificado.</p>";        
    }
    
    $config = new Configuracao();
    $config->setName('EMAILS_CONTATO');
    $config->setValue($_REQUEST['EMAILS_CONTATO']);

    ConfiguracoesDAO::setObject($config);
    ConfiguracoesDAO::DBConnection();
    
    if (ConfiguracoesDAO::updateItem())
        $outputMessage.="<p class='jcms-msg-ok'>E-mails para receber contato atualizados com sucesso!</p>";
    else
        $outputMessage.="<p class='jcms-msg-error'>Erro ao tentar e-mails de contato!<br/>ERROR: ".$config->getErrorMsg()."</p>";    

    $config = new Configuracao();
    $config->setName('RECIPS_PER_EMAIL');
    $config->setValue($_REQUEST['RECIPS_PER_EMAIL']);

    ConfiguracoesDAO::setObject($config);
    ConfiguracoesDAO::DBConnection();
    
    if (ConfiguracoesDAO::updateItem())
        $outputMessage.="<p class='jcms-msg-ok'>Destinat�rios por e-mail atualizado com sucesso!</p>";
    else
        $outputMessage.="<p class='jcms-msg-error'>Erro ao tentar atualizar Destinat�rios por e-mail!<br/>ERROR: ".$config->getErrorMsg()."</p>";    
    
    if (!empty($outputMessage))
        $_SESSION['output_message']=$outputMessage;        


?>