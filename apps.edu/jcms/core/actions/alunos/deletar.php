<?php

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");
    
    Forms::setFormName("frm-delete-aluno");
    
    $alunoID = (int) $_REQUEST['id'];
	
    $sucess=false;
        
    if (!empty($alunoID)) {		
        
        $aluno = new Aluno($alunoID);
        AlunosDAO::setObject($aluno);        
        AlunosDAO::DBConnection();
        AlunosDAO::getObjectDBData();
        
        if (AlunosDAO::deleteItem()) {
            $outputMessage="<p class='jcms-msg-ok'>Aluno exclu�do com sucesso!</p>";
            $sucess=true;
        }
        else
            $outputMessage="<p class='jcms-msg-error'>Erro ao tentar excluir Im�vel.<br/>".$aluno->getErrorMsg()."</p>";                 
    }
    else
        $outputMessage="<p class='jcms-msg-error'>Im�vel n�o excluido!<br/>Registro n�o localizado.</p>"; 

    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage); 


?>