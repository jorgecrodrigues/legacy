<?php

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");   
    
    Forms::setFormName("frm-edit-aluno");
    
    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;	
    $aluno = new Aluno($id);
    
    $nome = $_REQUEST['nome'];
    $alias = $_REQUEST['alias'];
    $jogo_id = $_REQUEST['jogo'];
    
    $sucess=false;
    
    if (!empty($nome) && !empty($alias) && !empty($jogo_id)) {
    
        $aluno->setNome($nome);
        $aluno->setAlias(trim($alias));
        $aluno->setJogoID($jogo_id);
        $aluno->setGrupo($_REQUEST['grupo']);
        $aluno->setPtsSemanas($_REQUEST['pontos-semana']);
        $aluno->setTotalPontos($_REQUEST['total-pontos']);
        
        AlunosDAO::setObject($aluno);
        AlunosDAO::DBConnection();
        
        if (AlunosDAO::updateItem()) {
            $outputMessage="<p class='jcms-msg-ok'>Aluno atualizado com sucesso!</p>";
            $alunoMedalhas = new AlunoMedalhas();
            $alunoMedalhas->setAlunoID($aluno->getAlunoID());
            AlunosMedalhasDAO::setObject($alunoMedalhas);
            AlunosMedalhasDAO::deleteItem();
            
            if (sizeof($_REQUEST['medalhas']) > 0) {
                $alunoMedalhas->setMedalhasID($_REQUEST['medalhas']);
                AlunosMedalhasDAO::insertItem();
            } 			
            $sucess=true;
        }
        else
            $outputMessage="<p class='jcms-msg-error'>N�o foi poss�vel atualizar aluno!<br/>Error: ".$aluno->getErrorMsg()."</p>";	
    
    }
    else
        $outputMessage="<p class='jcms-msg-error'>Aluno n�o atualizado!<br/>Campos marcados com * s�o obrigat�rios.</p>";		

    if (!$sucess)
        Forms::setFormData($_REQUEST);
        
    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);      


?>