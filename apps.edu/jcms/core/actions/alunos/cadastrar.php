<?php

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");   
    
    Forms::setFormName("frm-add-aluno");
    
    $nome = $_REQUEST['nome'];
    $alias = $_REQUEST['alias'];
    $jogo_id = (int) $_REQUEST['jogo'];
    
    $sucess=false;
    
    if (!empty($nome) && !empty($alias) && !empty($jogo_id)) {
    
        $aluno = new Aluno();
        $aluno->setNome($nome);
        $aluno->setAlias(trim($alias));
        $aluno->setJogoID($jogo_id);
        $aluno->setGrupo($_REQUEST['grupo']);
        
        AlunosDAO::setObject($aluno);
        AlunosDAO::DBConnection();
        
        if (AlunosDAO::insertItem()) {
            $outputMessage="<p class='jcms-msg-ok'>Aluno cadastrado com sucesso!</p>";
            $sucess=true;
        }
        else
            $outputMessage="<p class='jcms-msg-error'>N�o foi poss�vel cadastrar aluno!<br/>Error: ".$aluno->getErrorMsg()."</p>";	
    
    }
    else
        $outputMessage="<p class='jcms-msg-error'>Aluno n�o cadastrado!<br/>Campos marcados com * s�o obrigat�rios.</p>";		

    if (!$sucess)
        Forms::setFormData($_REQUEST);
        
    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);      


?>