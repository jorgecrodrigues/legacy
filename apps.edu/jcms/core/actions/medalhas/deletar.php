<?php

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");
    
    $medalhaID = (int) $_REQUEST['id'];
    
    Forms::setFormName("frm-delete-medalha");
    
    if (!empty($medalhaID)) {
        
        $medalha = new Medalha($medalhaID);
        MedalhasDAO::setObject($medalha);
        MedalhasDAO::DBConnection();
        MedalhasDAO::getObjectDBData();
        
        if (MedalhasDAO::deleteItem()) {
            if (($medalha->getImagem()) && is_file("../../../imagens/medalhas/".$medalha->getImagem())) {                  
                File::remove("../../../imagens/medalhas/".$medalha->getImagem());
            }			
            $outputMessage="<p class='jcms-msg-ok'>Medalha excluida com sucesso!</p>";
        }
        else
            $outputMessage="<p class='jcms-msg-error'>Erro ao tentar excluir Medalha.<br/>".$medalha->getErrorMsg()."</p>";                 
    }
    else
        $outputMessage="<p class='jcms-msg-error'>Medalha n�o localizado.</p>"; 

    if (!$sucess)
        Forms::setFormData($_REQUEST);
        
    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);

?>