<?php

    set_time_limit(0);

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");
    
    $nome=$_REQUEST['nome'];
	$jogo_id=(int) $_REQUEST['jogo'];
	$imagem = $_FILES['imagem'];
    
    Forms::setFormName("frm-add-medalha");
    
    $sucess = false;
    
    if (!empty($nome) && !empty($jogo_id) && !empty($imagem['name'])) {
    
        $medalha = new Medalha();
		$medalha->setJogoID($jogo_id);
		$medalha->setNome($nome);
		$medalha->setDescricao($_REQUEST['descricao']);
		MedalhasDAO::setObject($medalha);
		MedalhasDAO::DBConnection();
        MedalhasDAO::setLastOrdem();
		
        if (MedalhasDAO::insertItem()) {
            $outputMessage="<p class='jcms-msg-ok'>Medalha cadastrada com sucesso!</p>";
            $sucess = true;
            
			if (File::isImage($imagem['name'])) {
				$imagens_path = "../../../imagens/medalhas/";
				$new_file_name = "img-"."medal-".$medalha->getLastInsertedID()."-".md5(uniqid(rand(), true)).".".File::extension($imagem['name']);
				if (File::put($imagem['tmp_name'],$imagens_path.$new_file_name)) {
					$medalha->setMedalhaID($medalha->getLastInsertedID());
					$medalha->setImagem($new_file_name);
					MedalhasDAO::atualizarImagem();
				}
			}          
            
        }
        else
            $outputMessage="<p class='jcms-msg-error'>N�o foi poss�vel cadastrar Medalha!<br/>Error: ".$artigo->getErrorMsg()."</p>";	
    
    }
    else
        $outputMessage="<p class='jcms-msg-error'>Medalha n�o cadastrada!<br/>Campos marcados com * s�o obrigat�rios.</p>";		

    if (!$sucess)
        Forms::setFormData($_REQUEST);
        
    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);   

?>