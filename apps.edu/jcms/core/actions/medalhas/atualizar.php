<?php

    set_time_limit(0);

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");
		
	$id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;
	$medalha = new Medalha($id);
    
    $nome=$_REQUEST['nome'];
	$jogo_id=(int) $_REQUEST['jogo'];
    
    Forms::setFormName("frm-edit-medalha");
    
    $sucess = false;
    
    if (!empty($nome) && !empty($jogo_id)) {
    
		$medalha->setJogoID($jogo_id);
		$medalha->setNome($nome);
		$medalha->setOrdem($_REQUEST['ordem']);
		$medalha->setDescricao($_REQUEST['descricao']);
		MedalhasDAO::setObject($medalha);
		MedalhasDAO::DBConnection();
		
        if (MedalhasDAO::updateItem()) {
            $outputMessage="<p class='jcms-msg-ok'>Medalha atualizada com sucesso!</p>";
            $sucess = true;
			
			if (!empty($_FILES['imagem']['name'])) {
                $imagem = $_FILES['imagem'];
                if (File::isImage($imagem['name'])) {
                    
					$imagens_path = "../../../imagens/medalhas/";
                    
                    if (!empty($_REQUEST['imagem-atual']) && is_file($imagens_path.$_REQUEST['imagem-atual'])) {
                        File::remove($imagens_path.$_REQUEST['imagem-atual']);
                    }
                    
					$new_file_name = "img-"."medal-".$medalha->getMedalhaID()."-".md5(uniqid(rand(), true)).".".File::extension($imagem['name']);
					if (File::put($imagem['tmp_name'],$imagens_path.$new_file_name)) {
						$medalha->setImagem($new_file_name);
					}
					
                }
            }
            else
                $medalha->setImagem($_REQUEST['imagem-atual']);
                
            MedalhasDAO::atualizarImagem();			
            
        }
        else
            $outputMessage="<p class='jcms-msg-error'>N�o foi poss�vel cadastrar Medalha!<br/>Error: ".$artigo->getErrorMsg()."</p>";	
    
    }
    else
        $outputMessage="<p class='jcms-msg-error'>Medalha n�o cadastrada!<br/>Campos marcados com * s�o obrigat�rios.</p>";		

    if (!$sucess)
        Forms::setFormData($_REQUEST);
        
    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);   

?>