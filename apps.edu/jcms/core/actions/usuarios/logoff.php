<?php

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");
    
    if (isset($_SESSION['instances']['backend']['user'])) {
        $usuario = unserialize($_SESSION['instances']['backend']['user']);
        $usuario->logoff("backend");
        unset($usuario);
    }

?>