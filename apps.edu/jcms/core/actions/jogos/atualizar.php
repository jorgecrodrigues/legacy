<?php

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");   
    
    Forms::setFormName("frm-edit-jogo");
    
    $id = !empty($_REQUEST['id']) ? (int) $_REQUEST['id'] : null;	
    
	$sucess=false;
	
	$jogo = new Jogo($id);
    JogosDAO::setObject($jogo);
    JogosDAO::DBConnection();
    JogosDAO::getObjectDBData(); 	
    
	$jogo->setNome($_REQUEST['nome']);
	$jogo->setSigla($_REQUEST['sigla']);
	$jogo->setLabelEtapas($_REQUEST['label-etapas']);
	
	$ranking_pts = (int) $_REQUEST['chk-rkg-pts'];
	$ranking_pts_grupo = (int) $_REQUEST['chk-rkg-pts-grupo'];
	$ranking_medals = (int) $_REQUEST['chk-rkg-medals'];
	$ranking_medals_grupo = (int) $_REQUEST['chk-rkg-medals-grupo'];	
	$ranking = $ranking_pts.$ranking_pts_grupo.$ranking_medals.$ranking_medals_grupo;
	
	$jogo->setRanking($ranking);
    $jogo->setAvisos($_REQUEST['avisos']);
	$jogo->setPtsSemanas($_REQUEST['pontos-semana']);
	$jogo->setAtiva($_REQUEST['jogo-ativo']);
	
	JogosDAO::setObject($jogo);
	JogosDAO::DBConnection();
	
	if (JogosDAO::updateItem()) {
		$outputMessage="<p class='jcms-msg-ok'>Jogo atualizado com sucesso!</p>";
		$sucess=true;
	}
	else
		$outputMessage="<p class='jcms-msg-error'>N�o foi poss�vel atualizar jogo!<br/>Error: ".$jogo->getErrorMsg()."</p>";	

    if (!$sucess)
        Forms::setFormData($_REQUEST);
        
    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);      


?>