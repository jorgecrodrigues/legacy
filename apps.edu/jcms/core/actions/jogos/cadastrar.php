<?php

    if (!defined("INDEX"))
        die("Esse script n�o pode ser acessado diretamente!");   
    
    Forms::setFormName("frm-add-jogo");
    
	$sucess=false;
	
	$jogo = new Jogo();
    JogosDAO::setObject($jogo);
    JogosDAO::DBConnection();
    JogosDAO::getObjectDBData(); 	
    
	$nome          =   $_REQUEST['nome'];
	$sigla         =   $_REQUEST['sigla'];
	$label_etapas  =   $_REQUEST['label-etapas'];
	
	if (!empty($nome) && !empty($sigla) && !empty($label_etapas)) {
		$jogo->setNome($nome);
		$jogo->setSigla($sigla);
		$jogo->setLabelEtapas($label_etapas);
		
		$ranking_pts = (int) $_REQUEST['chk-rkg-pts'];
		$ranking_pts_grupo = (int) $_REQUEST['chk-rkg-pts-grupo'];
		$ranking_medals = (int) $_REQUEST['chk-rkg-medals'];
		$ranking_medals_grupo = (int) $_REQUEST['chk-rkg-medals-grupo'];	
		$ranking = $ranking_pts.$ranking_pts_grupo.$ranking_medals.$ranking_medals_grupo;
		
		$jogo->setRanking($ranking);
		
		JogosDAO::setObject($jogo);
		JogosDAO::DBConnection();
		
		if (JogosDAO::insertItem()) {
			$outputMessage="<p class='jcms-msg-ok'>Jogo cadastrado com sucesso!</p>";
			$sucess=true;
		}
		else {
			$outputMessage="<p class='jcms-msg-error'>N�o foi poss�vel cadastrar jogo!<br/>Error: ".$jogo->getErrorMsg()."</p>";			
		}
	}
	else
		$outputMessage="<p class='jcms-msg-error'>Jogo n�o cadastrado!<br />Preencha todos os campos obrigat�rios.</p>";
	
    if (!$sucess)
        Forms::setFormData($_REQUEST);
        
    Forms::status($sucess);
    
    if (isset($outputMessage))
        Forms::setOutputMessage($outputMessage);      


?>