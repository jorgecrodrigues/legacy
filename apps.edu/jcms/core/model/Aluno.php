<?php

    class Aluno extends ModelObject {

	private $alunoID;
	private $jogoID;
	private $nome;
	private $alias;
	private $grupo;
	private $ptsSem1;
	private $ptsSem2;
	private $ptsSem3;
	private $ptsSem4;
	private $ptsSem5;
	private $ptsSem6;
	private $ptsSem7;
	private $ptsSem8;
	private $ptsSem9;
	private $ptsSem10;
	private $ptsSem11;
	private $ptsSem12;
	private $ptsSem13;
	private $ptsSem14;
	private $ptsSem15;
    private $totalPontos;
	private $totalMedalhas;

	/*
		getters and setters
	*/
	public function __construct($id=null) {
		if (!empty($id))
		$this->alunoID=$id;
	}

	public function getAlunoID() { return $this->alunoID; }
	public function getJogoID() { return $this->jogoID; }
	public function getNome() { return $this->nome; }
	public function getAlias() { return $this->alias; }
	public function getGrupo() { return $this->grupo; }
	public function getPtsSem1() { return $this->ptsSem1; }
	public function getPtsSem2() { return $this->ptsSem2; }
	public function getPtsSem3() { return $this->ptsSem3; }
	public function getPtsSem4() { return $this->ptsSem4; }
	public function getPtsSem5() { return $this->ptsSem5; }
	public function getPtsSem6() { return $this->ptsSem6; }
	public function getPtsSem7() { return $this->ptsSem7; }
	public function getPtsSem8() { return $this->ptsSem8; }
	public function getPtsSem9() { return $this->ptsSem9; }
	public function getPtsSem10() { return $this->ptsSem10; }
	public function getPtsSem11() { return $this->ptsSem11; }
	public function getPtsSem12() { return $this->ptsSem12; }
	public function getPtsSem13() { return $this->ptsSem13; }
	public function getPtsSem14() { return $this->ptsSem14; }
	public function getPtsSem15() { return $this->ptsSem15; }
    public function getTotalPontos() { return $this->totalPontos; }
	public function getTotalMedalhas() { return $this->totalMedalhas; }

	public function setAlunoID($alunoID) { $this->alunoID=$alunoID; }
	public function setJogoID($jogoID) { $this->jogoID=$jogoID; }
	public function setNome($nome) { $this->nome=$nome; }
	public function setAlias($alias) { $this->alias=$alias; }
	public function setGrupo($grupo) { $this->grupo=$grupo; }
	public function setPtsSem1($ptsSem1) { $this->ptsSem1=$ptsSem1; }
	public function setPtsSem2($ptsSem2) { $this->ptsSem2=$ptsSem2; }
	public function setPtsSem3($ptsSem3) { $this->ptsSem3=$ptsSem3; }
	public function setPtsSem4($ptsSem4) { $this->ptsSem4=$ptsSem4; }
	public function setPtsSem5($ptsSem5) { $this->ptsSem5=$ptsSem5; }
	public function setPtsSem6($ptsSem6) { $this->ptsSem6=$ptsSem6; }
	public function setPtsSem7($ptsSem7) { $this->ptsSem7=$ptsSem7; }
	public function setPtsSem8($ptsSem8) { $this->ptsSem8=$ptsSem8; }
	public function setPtsSem9($ptsSem9) { $this->ptsSem9=$ptsSem9; }
	public function setPtsSem10($ptsSem10) { $this->ptsSem10=$ptsSem10; }
	public function setPtsSem11($ptsSem11) { $this->ptsSem11=$ptsSem11; }
	public function setPtsSem12($ptsSem12) { $this->ptsSem12=$ptsSem12; }
	public function setPtsSem13($ptsSem13) { $this->ptsSem13=$ptsSem13; }
	public function setPtsSem14($ptsSem14) { $this->ptsSem14=$ptsSem14; }
	public function setPtsSem15($ptsSem15) { $this->ptsSem15=$ptsSem15; }
    public function setTotalPontos($totalPontos) { $this->totalPontos=$totalPontos; }
	public function setTotalMedalhas($totalMedalhas) { $this->totalMedalhas=$totalMedalhas; } 
        
	public function setPtsSemanas(&$pts) {
		$sem="";
		for ($i=1;$i<=15;$i++) {
			if (isset($pts[$i]) && !empty($pts[$i])) {
				$sem="setPtsSem".$i;
				$this->$sem($pts[$i]);
			}
		}
	}
        
    }

?>