<?php

	class Medalha extends ModelObject {

		private $medalhaID;
		private $jogoID;
		private $ordem;
		private $nome;
		private $descricao;
		private $imagem;
	
		/*
			getters and setters
		*/

		public function __construct($id=null) {
			if (!empty($id))
				$this->medalhaID=$id;
		}
	
		public function getMedalhaID() { return $this->medalhaID; }
		public function getJogoID() { return $this->jogoID; }
		public function getOrdem() { return $this->ordem; }
		public function getNome() { return $this->nome; }
		public function getDescricao() { return $this->descricao; }
		public function getImagem() { return $this->imagem; }
	
		public function setMedalhaID($medalhaID) { $this->medalhaID=$medalhaID; }
		public function setJogoID($jogoID) { $this->jogoID=$jogoID; }
		public function setOrdem($ordem) { $this->ordem=$ordem; }
		public function setNome($nome) { $this->nome=$nome; }
		public function setDescricao($descricao) { $this->descricao=$descricao; }
		public function setImagem($imagem) { $this->imagem=$imagem; }

	}

?>