<?php

class ConfiguracoesDAO extends DAO {

    private static $object;
    private static $tbName="tb_configuracoes";

    public static function getObjectDBData () {
        $configID=self::getObject()->getConfigID();
        if (!empty($configID))
            $filter = "config_id='$configID'";
        else {
            $name = parent::scapeString(self::getObject()->getName());
            $filter = "name='".$name."'";
        }
        $sql = "SELECT * FROM ".self::$tbName." WHERE ".$filter;
        if (self::executeQuery($sql,self::getObject())) {
            if (self::getObject()->getNumRows() > 0)
                self::fillObject();
            else
                self::getObject()->setErrorCode(404);
            
            return 1;
        }
        return 0;
    }

    public static function fillObject () {
        $resultData = self::fetchQueryResult(self::getObject()->getQueryResult(),"assoc");
        self::getObject()->setConfigID($resultData['config_id']);
        self::getObject()->setName($resultData['name']);
        self::getObject()->setValue($resultData['value']);
    }

    public static function insertItem() {
        $configID=(int) self::getObject()->getConfigID();
        $name=parent::scapeString(self::getObject()->getName());
        $value=parent::scapeString(self::getObject()->getValue());

        $sql = "INSERT INTO ".
                self::$tbName."(name,value)".
                "VALUES ('".$name."', '".$value."')";

        if (self::executeQuery($sql,self::getObject()))
            return 1;
        else
            return 0;
    }

    public static function updateItem() {
        $configID=(int) self::getObject()->getConfigID();
        $name=parent::scapeString(self::getObject()->getName());
        $value=parent::scapeString(self::getObject()->getValue());

        $sql = "UPDATE ".self::$tbName
                ." SET
                        value='".$value."'
                 WHERE  name='".$name."'";

        if (self::executeQuery($sql,self::getObject()))
            return 1;
        else
            return 0;
    }

    public static function deleteItem() {
        $configID=(int) self::getObject()->getConfigID();
        $sql = "DELETE FROM ".self::$tbName." WHERE config_id='".$configID."'";

        if (self::executeQuery($sql,self::getObject()))
            return 1;
        else
            return 0;
    }

    public static function listItems(&$sorterBy, &$filterBy, $rowIndexIni, $rowIndexEnd) {
        return parent::listItems(self::$object, self::$tbName, $sorterBy, $filterBy, $rowIndexIni, $rowIndexEnd);
    }

    public static function getObject()  { return self::$object; }
    public static function getTbName() { return self::$tbName; }

    public static function setObject(&$obj)  { self::$object = $obj;   }
    public static function setTbName($tbName) { self::$tbName = $tbName; }


}    
    

?>