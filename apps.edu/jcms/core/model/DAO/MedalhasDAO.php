<?php

	class MedalhasDAO extends DAO {

		private static $object;
		private static $tbName="tb_medalhas";
		public static $sorterBy=array();
		public static $filterBy=array();
	
		public static function getObjectDBData () {
			$medalhaID=self::getObject()->getMedalhaID();
			$filter = "medalha_id='$medalhaID'";
			$sql = "SELECT * FROM ".self::$tbName." WHERE ".$filter;
			if (self::executeQuery($sql,self::getObject())) {
				if (self::getObject()->getNumRows() > 0)
					self::fillObject();
				else
					self::getObject()->setErrorCode(404);
				
				return 1;
			}
			return 0;
		}
	
		public static function fillObject () {
			$resultData = self::fetchQueryResult(self::getObject()->getQueryResult(),"assoc");
			self::getObject()->setMedalhaID($resultData['medalha_id']);
			self::getObject()->setJogoID($resultData['jogo_id']);
			self::getObject()->setOrdem($resultData['ordem']);
			self::getObject()->setNome(stripslashes($resultData['nome']));
			self::getObject()->setDescricao(stripslashes($resultData['descricao']));
			self::getObject()->setImagem(stripslashes($resultData['imagem']));
		}
	
		public static function insertItem() {
			$jogoID=(int) self::getObject()->getJogoID();
			$ordem=(int) self::getObject()->getOrdem();
			$nome=parent::scapeString(self::getObject()->getNome());
			$descricao=parent::scapeString(self::getObject()->getDescricao());
			$imagem=parent::scapeString(self::getObject()->getImagem());
	
			$sql = "INSERT INTO ".
				self::$tbName."(jogo_id,ordem,nome,descricao,imagem)".
				"VALUES ('".$jogoID."', '".$ordem."', '".$nome."', '".$descricao."', '".$imagem."')";
		
			if (self::executeQuery($sql,self::getObject()))
				return 1;
			else
				return 0;
		}
	
		public static function updateItem() {
			$medalhaID=(int) self::getObject()->getMedalhaID();
			$jogoID=(int) self::getObject()->getJogoID();
			$ordem=(int) self::getObject()->getOrdem();
			$nome=parent::scapeString(self::getObject()->getNome());
			$descricao=parent::scapeString(self::getObject()->getDescricao());
			$imagem=parent::scapeString(self::getObject()->getImagem());
	
			$sql = "UPDATE ".self::$tbName
				." SET
					jogo_id='".$jogoID."',
					ordem='".$ordem."',
					nome='".$nome."',
					descricao='".$descricao."',
					imagem='".$imagem."'
				 WHERE medalha_id='".$medalhaID."'";
		
			if (self::executeQuery($sql,self::getObject()))
				return 1;
			else
				return 0;
		}
	
		public static function deleteItem() {
			$medalhaID=(int) self::getObject()->getMedalhaID();
			$sql = "DELETE FROM ".self::$tbName." WHERE medalha_id='".$medalhaID."'";
		
			if (self::executeQuery($sql,self::getObject()))
				return 1;
			else
				return 0;
		}
		
		public static function setLastOrdem() {
			$jogoID=(int) self::getObject()->getJogoID();
			$sql = "SELECT MAX(ordem) FROM ".self::$tbName." WHERE jogo_id=".$jogoID;
			
			if (self::executeQuery($sql,self::getObject())) {
				$resultData = self::fetchQueryResult(self::getObject()->getQueryResult(),"num");
				$ordem = intval($resultData[0])+1;
				self::getObject()->setOrdem($ordem);
				return 1;
			}
			else {
				return 0;
			}
		}
		
		public static function atualizarImagem() {
			$medalhaID=(int) self::getObject()->getMedalhaID();
			$imagem = self::getObject()->getImagem();
			$sql = "UPDATE ".self::$tbName." SET imagem='".$imagem."' WHERE medalha_id='".$medalhaID."'";			
			if (self::executeQuery($sql,self::getObject()))
				return 1;
			else
				return 0;
		}		
	
		public static function listItems($rowIndexIni, $rowIndexEnd) {
			return parent::listItems(self::$object, self::$tbName, self::$sorterBy, self::$filterBy, $rowIndexIni, $rowIndexEnd);
		}
	
		public static function addSorter($param) {
			array_push(self::$sorterBy,$param);
		}
	
		public static function addFilter($param) {
			array_push(self::$filterBy,$param);
		}
	
		public static function clearSorter() {
			self::$sorterBy=array();
		}
	
		public static function clearFilter() {
			self::$filterBy=array();
		}
	
		public static function getObject()  { return self::$object; }
		public static function getTbName() { return self::$tbName; }
	
		public static function setObject(&$obj)  { self::$object = $obj;   }
		public static function setTbName($tbName) { self::$tbName = $tbName; }
	
	}

?>