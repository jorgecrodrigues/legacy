<?php

    class AlunosDAO extends DAO {

	private static $object;
	private static $tbName="tb_todos_alunos";
	public static $sorterBy=array();
	public static $filterBy=array();

	public static function getObjectDBData () {
	    $alunoID=self::getObject()->getAlunoID();
		if (!empty($alunoID))        
			$filter = "aluno_id='$alunoID'";
		else {
			$alias = parent::scapeString(self::getObject()->getAlias());
			$filter = "alias='".$alias."'";
		}
	    $sql = "SELECT * FROM ".self::$tbName." WHERE ".$filter;
	    if (self::executeQuery($sql,self::getObject())) {
	        if (self::getObject()->getNumRows() > 0)
	            self::fillObject();
	        else
	            self::getObject()->setErrorCode(404);
	        
	        return 1;
	    }
	    return 0;
	}

	public static function fillObject () {
		$resultData = self::fetchQueryResult(self::getObject()->getQueryResult(),"assoc");
		self::getObject()->setAlunoID($resultData['aluno_id']);
		self::getObject()->setJogoID($resultData['jogo_id']);
		self::getObject()->setNome(stripslashes($resultData['nome']));
		self::getObject()->setAlias(stripslashes($resultData['alias']));
		self::getObject()->setGrupo(stripslashes($resultData['grupo']));
		self::getObject()->setPtsSem1($resultData['pts_sem1']);
		self::getObject()->setPtsSem2($resultData['pts_sem2']);
		self::getObject()->setPtsSem3($resultData['pts_sem3']);
		self::getObject()->setPtsSem4($resultData['pts_sem4']);
		self::getObject()->setPtsSem5($resultData['pts_sem5']);
		self::getObject()->setPtsSem6($resultData['pts_sem6']);
		self::getObject()->setPtsSem7($resultData['pts_sem7']);
		self::getObject()->setPtsSem8($resultData['pts_sem8']);
		self::getObject()->setPtsSem9($resultData['pts_sem9']);
		self::getObject()->setPtsSem10($resultData['pts_sem10']);
		self::getObject()->setPtsSem11($resultData['pts_sem11']);
		self::getObject()->setPtsSem12($resultData['pts_sem12']);
		self::getObject()->setPtsSem13($resultData['pts_sem13']);
		self::getObject()->setPtsSem14($resultData['pts_sem14']);
		self::getObject()->setPtsSem15($resultData['pts_sem15']);
		self::getObject()->setTotalPontos($resultData['total_pontos']);
		self::getObject()->setTotalMedalhas($resultData['total_medalhas']);
	}

	public static function insertItem() {
            $nome=parent::scapeString(self::getObject()->getNome());
            $alias=parent::scapeString(self::getObject()->getAlias());
            $grupo=parent::scapeString(self::getObject()->getGrupo());
			$jogoID=(int) self::getObject()->getJogoID();

            $sql = "INSERT INTO ".
                    self::$tbName."(nome,jogo_id,alias,grupo)".
                    "VALUES ('".$nome."', '".$jogoID."', '".$alias."', '".$grupo."')";

            if (self::executeQuery($sql,self::getObject()))
                return 1;
            else
                return 0;
	}

	public static function updateItem() {
            $alunoID=(int) self::getObject()->getAlunoID();
			$jogoID=(int) self::getObject()->getJogoID();
            $nome=parent::scapeString(self::getObject()->getNome());
            $alias=parent::scapeString(self::getObject()->getAlias());
            $grupo=parent::scapeString(self::getObject()->getGrupo());
            $ptsSem1=(int) self::getObject()->getPtsSem1();
            $ptsSem2=(int) self::getObject()->getPtsSem2();
            $ptsSem3=(int) self::getObject()->getPtsSem3();
            $ptsSem4=(int) self::getObject()->getPtsSem4();
            $ptsSem5=(int) self::getObject()->getPtsSem5();
            $ptsSem6=(int) self::getObject()->getPtsSem6();
            $ptsSem7=(int) self::getObject()->getPtsSem7();
            $ptsSem8=(int) self::getObject()->getPtsSem8();
            $ptsSem9=(int) self::getObject()->getPtsSem9();
            $ptsSem10=(int) self::getObject()->getPtsSem10();
            $ptsSem11=(int) self::getObject()->getPtsSem11();
            $ptsSem12=(int) self::getObject()->getPtsSem12();
            $ptsSem13=(int) self::getObject()->getPtsSem13();
            $ptsSem14=(int) self::getObject()->getPtsSem14();
            $ptsSem15=(int) self::getObject()->getPtsSem15();
            $totalPontos=(int) self::getObject()->getTotalPontos();

            $sql = "UPDATE ".self::$tbName
                    ." SET
							jogo_id='".$jogoID."',
                            nome='".$nome."',
                            alias='".$alias."',
                            grupo='".$grupo."',
                            pts_sem1='".$ptsSem1."',
                            pts_sem2='".$ptsSem2."',
                            pts_sem3='".$ptsSem3."',
                            pts_sem4='".$ptsSem4."',
                            pts_sem5='".$ptsSem5."',
                            pts_sem6='".$ptsSem6."',
                            pts_sem7='".$ptsSem7."',
                            pts_sem8='".$ptsSem8."',
                            pts_sem9='".$ptsSem9."',
                            pts_sem10='".$ptsSem10."',
                            pts_sem11='".$ptsSem11."',
                            pts_sem12='".$ptsSem12."',
                            pts_sem13='".$ptsSem13."',
                            pts_sem14='".$ptsSem14."',
                            pts_sem15='".$ptsSem15."',
                            total_pontos='".$totalPontos."'
                     WHERE aluno_id='".$alunoID."'";
    
            if (self::executeQuery($sql,self::getObject()))
                return 1;
            else
                return 0;
	}

	public static function deleteItem() {
		$alunoID=(int) self::getObject()->getAlunoID();
		$sql = "DELETE FROM ".self::$tbName." WHERE aluno_id='".$alunoID."'";

		if (self::executeQuery($sql,self::getObject()))
			return 1;
		else
			return 0;
	}
	
	public static function atualizaTotalMedalhas() {
		$alunoID=(int) self::getObject()->getAlunoID();
		$totalMedalhas=(int) self::getObject()->getTotalMedalhas();
		$sql = "UPDATE ".self::$tbName." SET total_medalhas='".$totalMedalhas."' WHERE aluno_id='".$alunoID."'";

		if (self::executeQuery($sql,self::getObject()))
			return 1;
		else
			return 0;		
	}
        
	public static function getGrupos() {
		$jogoID = (int) self::getObject()->getJogoID();
		if ($jogoID) {
			$sql = "SELECT DISTINCT grupo FROM tb_todos_alunos WHERE jogo_id='".$jogoID."' ORDER BY grupo ASC";
		}
		else {
			$sql = "SELECT DISTINCT grupo FROM tb_todos_alunos ORDER BY grupo ASC";
		}
		if (self::executeQuery($sql,self::getObject())) {
			$r = self::result2Array(self::getObject()->getQueryResult());
			return $r;
		}
		else
			return 0;
	}
        
	public static function rankingPtsByGroups($jogo_id) {
		$sql = "SELECT
					count( aluno_id ) AS membros,
					sum( total_pontos ) AS pontos,
					AVG(total_pontos) as media,
					grupo
				FROM tb_todos_alunos
					WHERE jogo_id = '".$jogo_id."'
				GROUP BY grupo
				ORDER BY media DESC";
		if (self::executeQuery($sql,self::getObject())) {
			$r = self::result2Array(self::getObject()->getQueryResult());
			return $r;
		}
		else
			return 0;                        
	}
	
	public static function rankingByMedalhas($jogo_id) {
		$sql = "SELECT 
					a.*, (IFNULL( COUNT( b.medalha_id ) , 0 )) as total_medalhas
				FROM
					tb_todos_alunos a LEFT OUTER JOIN tb_alunos_medalhas b ON a.aluno_id=b.aluno_id
				WHERE
					a.jogo_id='".$jogo_id."'
				GROUP BY a.aluno_id ORDER BY total_medalhas DESC";
		
		if (self::executeQuery($sql,self::getObject())) {
			return 1;
		}
		else
			return 0;                        
	}
	
	public static function rankingMedalhasByGroups($jogo_id) {
		$sql = "SELECT 
					a.grupo,
					(IFNULL( COUNT( b.medalha_id ) , 0 )) as medalhas_total,
					(
						SELECT
							count(aluno_id) AS membros
						FROM tb_todos_alunos a1
						WHERE jogo_id = '".$jogo_id."'
								and a.grupo=a1.grupo
						GROUP BY grupo) as membros,
					(SELECT (IFNULL( COUNT( b.medalha_id ) , 0 ))/membros) as media
				FROM 
					tb_todos_alunos a LEFT OUTER JOIN tb_alunos_medalhas b ON a.aluno_id=b.aluno_id
				WHERE a.jogo_id='".$jogo_id."'
				GROUP BY a.grupo
				ORDER BY media DESC";
		
		if (self::executeQuery($sql,self::getObject())) {
			$r = self::result2Array(self::getObject()->getQueryResult());
			return $r;
		}
		else
			return 0;                         
	}		

	public static function listItems($rowIndexIni, $rowIndexEnd) {
            return parent::listItems(self::$object, self::$tbName, self::$sorterBy, self::$filterBy, $rowIndexIni, $rowIndexEnd);
	}

	public static function addSorter($param) {
    	    array_push(self::$sorterBy,$param);
	}

	public static function addFilter($param) {
    	    array_push(self::$filterBy,$param);
	}

	public static function clearSorter() {
            self::$sorterBy=array();
	}

	public static function clearFilter() {
            self::$filterBy=array();
	}

	public static function getObject()  { return self::$object; }
	public static function getTbName() { return self::$tbName; }

	public static function setObject(&$obj)  { self::$object = $obj;   }
	public static function setTbName($tbName) { self::$tbName = $tbName; }

    } 

?>