<?php

    abstract class DAO extends DataBase {
        
        abstract public static function getObjectDBData();
        
        abstract public static function fillObject();
        
        abstract public static function insertItem();
        
        abstract public static function deleteItem();
        
        /* uma listagem simples dos dados de uma tabela */
        public static function listItems(&$object, $tbName, &$sorterBy, &$filterBy, $rowIndexIni, $rowIndexEnd, $where_clause=null) {
            if (is_array($sorterBy) && !empty($sorterBy)) {
                $sorter="ORDER BY ";
                $num = sizeof($sorterBy);
                foreach ($sorterBy as $k => $s) {
                    if ($k == ($num-1))
                        $sorter .= $s[0]." ".$s[1]." ";
                    else
                        $sorter .= $s[0]." ".$s[1].", ";
                }
            }
            if (empty($where_clause)) {
                if (is_array($filterBy) && !empty($filterBy)) {
                    $filter=" WHERE 1";
                    foreach ($filterBy as $f) {
                        $filter .= " AND ".$f[0]." ".$f[1]." '".$f[2]."'";
                    }
                }
            }
            else
                $filter = $where_clause;
            
            $rowIndexIni = (int) $rowIndexIni;
            $rowIndexEnd = (int) $rowIndexEnd;
                
            if (isset($rowIndexEnd) && $rowIndexEnd > 0) {
                $limit="LIMIT ".$rowIndexIni.",".$rowIndexEnd;
            }

            $sql = "SELECT COUNT(*) FROM ".$tbName.(!empty($filter) ? $filter : ";");
            
            if (parent::executeQuery($sql,$object)) {
                $result = parent::fetchQueryResult($object->getQueryResult(), "num");
                $numRowsWL = $result[0];
                $object->setNumRowsWL($numRowsWL);
                if ($numRowsWL > 0) {
                    
                    $sql = "SELECT * FROM ".$tbName." "
                            . (!empty($filter) ? $filter : null)
                            . " " . (!empty($sorter) ? $sorter : null)
                            . " " . (!empty($limit) ? $limit  : null);
                
                    if (parent::executeQuery($sql,$object))
                        return 1;
                    else            
                        return 0;
                    
                }
                else {
                    $object->setNumRows(0);
                    $object->setErrorCode(404);
                    return 1;
                }
            }
            else {
                return 0;
            }

        }
        
        public static function search(&$object, $fields, $main_sql, $sorterBy, $rowIndexIni, $rowIndexEnd) {
            if (is_array($sorterBy) && !empty($sorterBy)) {
                $sorter="ORDER BY ";
                foreach ($sorterBy as $s) {
                    $sorter .= $s[0]." ".$s[1]." ";
                }
            }
            
            $rowIndexIni = (int) $rowIndexIni;
            $rowIndexEnd = (int) $rowIndexEnd;
                
            if (isset($rowIndexEnd) && $rowIndexEnd > 0) {
                $limit="LIMIT ".$rowIndexIni.",".$rowIndexEnd;
            }
            
            $sql = "SELECT COUNT(*) ".$main_sql." ".(!empty($filter) ? $filter : ";");
            
            if (parent::executeQuery($sql,$object)) {
                $result = parent::fetchQueryResult($object->getQueryResult(), "num");
                $numRowsWL = $result[0];
                $object->setNumRowsWL($numRowsWL);
                
                if ($numRowsWL > 0) {
                    
                    $sql = "SELECT ".$fields." ".$main_sql." "
                            . " " . (!empty($sorter) ? $sorter : null)
                            . " " . (!empty($limit) ? $limit  : null);
                    
                    if (parent::executeQuery($sql,$object))
                        return 1;
                    else            
                        return 0;
                    
                }
                else {
                    $object->setNumRows(0);
                    $object->setErrorCode(404);
                    return 1;
                }
            }
            else {
                return 0;
            }            
            
        }
        
    }

?>