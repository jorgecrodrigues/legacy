<?php

	class JogosDAO extends DAO {
	
		private static $object;
		private static $tbName="tb_jogos";
		public static $sorterBy=array();
		public static $filterBy=array();
	
		public static function getObjectDBData () {
			$jogoID=self::getObject()->getJogoID();
			if (!empty($jogoID))        
				$filter = "jogo_id='$jogoID'";
			else {
				$sigla = parent::scapeString(self::getObject()->getSigla());
				$filter = "sigla='".$sigla."'";
			}			
			$sql = "SELECT * FROM ".self::$tbName." WHERE ".$filter;
			if (self::executeQuery($sql,self::getObject())) {
				if (self::getObject()->getNumRows() > 0)
					self::fillObject();
				else
					self::getObject()->setErrorCode(404);
				
				return 1;
			}
			return 0;
		}
	
		public static function fillObject () {
			$resultData = self::fetchQueryResult(self::getObject()->getQueryResult(),"assoc");
			self::getObject()->setJogoID($resultData['jogo_id']);
			self::getObject()->setNome(stripslashes($resultData['nome']));
			self::getObject()->setSigla(stripslashes($resultData['sigla']));
			self::getObject()->setLabelEtapas(stripslashes($resultData['label_etapas']));
			self::getObject()->setRanking(stripslashes($resultData['ranking']));
			self::getObject()->setAvisos(stripslashes($resultData['avisos']));
            self::getObject()->setPtsSem1($resultData['pts_sem1']);
            self::getObject()->setPtsSem2($resultData['pts_sem2']);
            self::getObject()->setPtsSem3($resultData['pts_sem3']);
            self::getObject()->setPtsSem4($resultData['pts_sem4']);
            self::getObject()->setPtsSem5($resultData['pts_sem5']);
            self::getObject()->setPtsSem6($resultData['pts_sem6']);
            self::getObject()->setPtsSem7($resultData['pts_sem7']);
            self::getObject()->setPtsSem8($resultData['pts_sem8']);
            self::getObject()->setPtsSem9($resultData['pts_sem9']);
            self::getObject()->setPtsSem10($resultData['pts_sem10']);
            self::getObject()->setPtsSem11($resultData['pts_sem11']);
            self::getObject()->setPtsSem12($resultData['pts_sem12']);
            self::getObject()->setPtsSem13($resultData['pts_sem13']);
            self::getObject()->setPtsSem14($resultData['pts_sem14']);
            self::getObject()->setPtsSem15($resultData['pts_sem15']);			
			self::getObject()->setAtiva($resultData['ativa']);
		}
	
		public static function insertItem() {
			$nome=parent::scapeString(self::getObject()->getNome());
			$sigla=parent::scapeString(self::getObject()->getSigla());
			$labelEtapas=parent::scapeString(self::getObject()->getLabelEtapas());
			$ranking=parent::scapeString(self::getObject()->getRanking());
			$ativa=(int) self::getObject()->getAtiva();
	
			$sql = "INSERT INTO ".
				self::$tbName."(nome,sigla,label_etapas,ranking,ativa)".
				"VALUES ('".$nome."', '".$sigla."', '".$labelEtapas."', '".$ranking."', '".$ativa."')";
		
			if (self::executeQuery($sql,self::getObject()))
				return 1;
			else
				return 0;
		}
	
		public static function updateItem() {
			$jogoID=(int) self::getObject()->getJogoID();
			$nome=parent::scapeString(self::getObject()->getNome());
			$sigla=parent::scapeString(self::getObject()->getSigla());
			$labelEtapas=parent::scapeString(self::getObject()->getLabelEtapas());
			$ranking=parent::scapeString(self::getObject()->getRanking());
			$avisos=parent::scapeString(self::getObject()->getAvisos());
			$ptsSem1=(int) self::getObject()->getPtsSem1();
            $ptsSem2=(int) self::getObject()->getPtsSem2();
            $ptsSem3=(int) self::getObject()->getPtsSem3();
            $ptsSem4=(int) self::getObject()->getPtsSem4();
            $ptsSem5=(int) self::getObject()->getPtsSem5();
            $ptsSem6=(int) self::getObject()->getPtsSem6();
            $ptsSem7=(int) self::getObject()->getPtsSem7();
            $ptsSem8=(int) self::getObject()->getPtsSem8();
            $ptsSem9=(int) self::getObject()->getPtsSem9();
            $ptsSem10=(int) self::getObject()->getPtsSem10();
            $ptsSem11=(int) self::getObject()->getPtsSem11();
            $ptsSem12=(int) self::getObject()->getPtsSem12();
            $ptsSem13=(int) self::getObject()->getPtsSem13();
            $ptsSem14=(int) self::getObject()->getPtsSem14();
            $ptsSem15=(int) self::getObject()->getPtsSem15();			
			$ativa=(int) self::getObject()->getAtiva();
	
			$sql = "UPDATE ".self::$tbName
				." SET
					nome='".$nome."',
					sigla='".$sigla."',
					label_etapas='".$labelEtapas."',
					ranking='".$ranking."',
					avisos='".$avisos."',
					pts_sem1='".$ptsSem1."',
					pts_sem2='".$ptsSem2."',
					pts_sem3='".$ptsSem3."',
					pts_sem4='".$ptsSem4."',
					pts_sem5='".$ptsSem5."',
					pts_sem6='".$ptsSem6."',
					pts_sem7='".$ptsSem7."',
					pts_sem8='".$ptsSem8."',
					pts_sem9='".$ptsSem9."',
					pts_sem10='".$ptsSem10."',
					pts_sem11='".$ptsSem11."',
					pts_sem12='".$ptsSem12."',
					pts_sem13='".$ptsSem13."',
					pts_sem14='".$ptsSem14."',
					pts_sem15='".$ptsSem15."',					
					ativa='".$ativa."'
				 WHERE jogo_id='".$jogoID."'";
		
			if (self::executeQuery($sql,self::getObject()))
				return 1;
			else
				return 0;
		}
	
		public static function deleteItem() {
			$jogoID=(int) self::getObject()->getJogoID();
			$sql = "DELETE FROM ".self::$tbName." WHERE jogo_id='".$jogoID."'";
		
			if (self::executeQuery($sql,self::getObject()))
				return 1;
			else
				return 0;
		}
	
		public static function listItems($rowIndexIni, $rowIndexEnd) {
			return parent::listItems(self::$object, self::$tbName, self::$sorterBy, self::$filterBy, $rowIndexIni, $rowIndexEnd);
		}
		
		public static function listByAliasAluno($alias) {
			$alias=parent::scapeString($alias);
			$sql = "SELECT a.*,b.aluno_id FROM tb_jogos a, tb_todos_alunos b
						WHERE 
							a.jogo_id=b.jogo_id
						AND
							b.alias='".$alias."'
						AND
							a.ativa=1 ORDER BY nome ASC";			

			if (self::executeQuery($sql,self::getObject()))
				return 1;
			else
				return 0;
			
			
		}
	
		public static function addSorter($param) {
			array_push(self::$sorterBy,$param);
		}
	
		public static function addFilter($param) {
			array_push(self::$filterBy,$param);
		}
	
		public static function clearSorter() {
			self::$sorterBy=array();
		}
	
		public static function clearFilter() {
			self::$filterBy=array();
		}
	
		public static function getObject()  { return self::$object; }
		public static function getTbName() { return self::$tbName; }
	
		public static function setObject(&$obj)  { self::$object = $obj;   }
		public static function setTbName($tbName) { self::$tbName = $tbName; }
	
	}  

?>