<?php


class AlunosMedalhasDAO extends DAO {

    private static $object;       
    private static $tbName="tb_alunos_medalhas";
    
    public static function getObjectDBData () {
        $aluno_id=(int) self::getObject()->getAlunoID();
        $filter = "aluno_id = '".$aluno_id."'";
        
        $sql = "SELECT a.medalha_id, a.nome
           FROM tb_medalhas a, tb_alunos_medalhas b
           WHERE ".$filter."
           AND a.medalha_id = b.medalha_id ORDER BY a.ordem ASC";            
        
        if (self::executeQuery($sql,self::getObject())) {
            if (self::getObject()->getNumRows() > 0)
                self::fillObject();
            else
                self::getObject()->setErrorCode(404);
            
            return 1;
        }
        return 0;
    }
    
    public static function fillObject () {
        $arrayMedalhas = self::result2Array(self::getObject()->getQueryResult());
        $medalhas = array();			
        $nomes = array();
        
        foreach ($arrayMedalhas as $a) {
            array_push($medalhas, $a['medalha_id']);
            array_push($nomes, $a['medalha']);
        }        
        
        self::getObject()->setMedalhasID($medalhas);
        self::getObject()->setMedalhas($nomes);
    }
    
    public static function insertItem() {
        $aluno_id = (int) self::getObject()->getAlunoID(); 
        $medalhas = self::getObject()->getMedalhasID();
        $sql = "INSERT INTO ".self::$tbName." (aluno_id, medalha_id) VALUES ";
        for ($i=0;$i<sizeof($medalhas);$i++) {
            $medalhaID = (int) $medalhas[$i];
            $sql .= "('".$aluno_id."', '".$medalhaID."'), ";
        }            
        $sql = substr($sql, 0, strlen($sql)-2);
        if (self::executeQuery($sql,self::getObject()))
            return 1;
        else
            return 0;
    }
            
    public static function deleteItem() {
        $aluno_id = (int) self::getObject()->getAlunoID();
        $sql = "DELETE FROM ".self::$tbName." WHERE aluno_id='".$aluno_id."'";			
        if (self::executeQuery($sql,self::getObject()))
            return 1;
        else
            return 0;
    }   
    
    public static function listItems(&$sorterBy, &$filterBy, $rowIndexIni, $rowIndexEnd) {
        return parent::listItems(self::$object, self::$tbName, $sorterBy, $filterBy, $rowIndexIni, $rowIndexEnd);
    }
    
    public static function getObject()  { return self::$object; }
    public static function getTbName() { return self::$tbName; }
    
    public static function setObject(&$obj)  { self::$object = $obj;   }
    public static function setTbName($tbName) { self::$tbName = $tbName; }         

}


?>