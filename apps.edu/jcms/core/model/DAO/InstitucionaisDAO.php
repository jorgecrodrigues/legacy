<?php

    class InstitucionaisDAO extends DAO {

	private static $object;
	private static $tbName="tb_institucionais";

	public static function getObjectDBData () {
	    $conteudoID=(int) self::getObject()->getConteudoID();
            if (!empty($conteudoID))
                $filter = "conteudo_id='$conteudoID'";
            else {
                $nome = parent::scapeString(self::getObject()->getNome());
                $filter = "nome='".$nome."'";
            }
	    $sql = "SELECT * FROM ".self::$tbName." WHERE ".$filter;
	    if (self::executeQuery($sql,self::getObject())) {
	        if (self::getObject()->getNumRows() > 0)
	            self::fillObject();
	        else
	            self::getObject()->setErrorCode(404);
	        
	        return 1;
	    }
	    return 0;
	}

        public static function fillObject () {
            $resultData = self::fetchQueryResult(self::getObject()->getQueryResult(),"assoc");
            self::getObject()->setConteudoID($resultData['conteudo_id']);
            self::getObject()->setNome(stripslashes($resultData['nome']));
            self::getObject()->setTitulo(stripslashes($resultData['titulo']));
            self::getObject()->setSubTitulo(stripslashes($resultData['sub_titulo']));
            self::getObject()->setChamada(stripslashes($resultData['chamada']));
            self::getObject()->setConteudo(stripslashes($resultData['conteudo']));
            self::getObject()->setImagemDestaque($resultData['imagem_destaque']);
	}

	public static function insertItem() {
            $nome=parent::scapeString(self::getObject()->getNome());
            $titulo=parent::scapeString(htmlentities(self::getObject()->getTitulo()));
            $subTitulo=parent::scapeString(htmlentities(self::getObject()->getSubTitulo()));
            $chamada=parent::scapeString(self::getObject()->getChamada());
            $conteudo=parent::scapeString(self::getObject()->getConteudo());

            $sql = "INSERT INTO ".
                    self::$tbName."(nome,titulo,sub_titulo,chamada,conteudo)".
                    "VALUES ('".$nome."', '".$titulo."', '".$subTitulo."', '".$chamada."', '".$conteudo."')";

            if (self::executeQuery($sql,self::getObject()))
                return 1;
            else
                return 0;
	}

	public static function updateItem() {
            $conteudoID=(int) self::getObject()->getConteudoID();
            $titulo=parent::scapeString(htmlentities(self::getObject()->getTitulo()));
            $subTitulo=parent::scapeString(htmlentities(self::getObject()->getSubTitulo()));
            $chamada=parent::scapeString(self::getObject()->getChamada());
            $conteudo=parent::scapeString(self::getObject()->getConteudo());

            $sql = "UPDATE ".self::$tbName
                    ." SET
                            titulo='".$titulo."',
                            sub_titulo='".$subTitulo."',
                            chamada='".$chamada."',
                            conteudo='".$conteudo."'
                     WHERE conteudo_id='".$conteudoID."'";

            if (self::executeQuery($sql,self::getObject()))
                return 1;
            else
                return 0;
	}

	public static function deleteItem() {
            $conteudoID=(int) self::getObject()->getConteudoID();
            $sql = "DELETE FROM ".self::$tbName." WHERE conteudo_id='".$conteudoID."'";

            if (self::executeQuery($sql,self::getObject()))
                return 1;
            else
                return 0;
	}
        
        public static function atualizarImagem() {
            $conteudoID=(int) self::getObject()->getConteudoID();
            $imagem_destaque = parent::scapeString(self::getObject()->getImagemDestaque());
            $sql = "UPDATE ".self::$tbName." SET imagem_destaque='".$imagem_destaque."' WHERE conteudo_id='".$conteudoID."'";			
            if (self::executeQuery($sql,self::getObject()))
                return 1;
            else
                return 0;
        }         

	public static function listItems(&$sorterBy, &$filterBy, $rowIndexIni, $rowIndexEnd) {
    	    return parent::listItems(self::$object, self::$tbName, $sorterBy, $filterBy, $rowIndexIni, $rowIndexEnd);
	}

	public static function getObject()  { return self::$object; }
	public static function getTbName() { return self::$tbName; }

	public static function setObject(&$obj)  { self::$object = $obj;   }
	public static function setTbName($tbName) { self::$tbName = $tbName; }



    }    

?>