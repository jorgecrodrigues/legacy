<?php

    class ModelObject {

        private $errorMsg;
        private $errorCode;
        private $resultNumRows;
        private $numRowsWL;
        private $queryResult;
        private $lastInsertedID;
    
        public function setErrorMsg($msg) { $this->errorMsg=$msg; }
        public function setErrorCode($code) { $this->errorCode=$code; }
        public function setLatIsertedID($id) { $this->lastInsertedID=$id; }
        public function setNumRows($num) { $this->resultNumRows=$num; }
        public function setNumRowsWL($num) { $this->numRowsWL=$num; }
        public function setQueryResult(&$result) { $this->queryResult=$result; }
    
        public function getErrorMsg()       { return $this->errorMsg;  	  }
        public function getErrorCode()      { return $this->errorCode;      }
        public function getLastInsertedID() { return $this->lastInsertedID; }
        public function getNumRows()   	    { return $this->resultNumRows;  }
        public function getNumRowsWL()   	    { return $this->numRowsWL;  }
        public function &getQueryResult()    { return $this->queryResult;    }

    }

?>