<?php

    class DataBase {
    
        private static $DBLink;
        private static $DBHost = DB_HOST;
        private static $DBName = DB_NAME;
        private static $DBUser = DB_USER;
        private static $DBPass = DB_PASS;
        private static $errorMsg;
        private static $errorCode;
    
        public static function DBConnection() {
            if (!isset(self::$DBLink)) {
                $DBLink = mysqli_connect(self::$DBHost, self::$DBUser, self::$DBPass);
                mysqli_select_db(self::$DBName, $DBLink);
                if ($DBLink) {
                    self::$DBLink=$DBLink;
                }
                else {
                    self::$errorCode=mysqli_errno();
                    self::$errorMsg=mysqli_error();
                    return 0;
                }
            }
        }
    
        /**
        * Executa uma query sob um determinado objeto
        */
        public static function executeQuery($sql, &$object) {
            if ($result = mysqli_query($sql, self::$DBLink)) {
                if (mysqli_insert_id(self::$DBLink)) {
                    $object->setLatIsertedID(mysqli_insert_id(self::$DBLink));
                }
                if (gettype($result)=="boolean") { $object->setNumRows(0); }
                else {
                    $object->setQueryResult($result);
                    $object->setNumRows(mysqli_num_rows($result));
                }
                return 1;
            }
            else {
                $object->setErrorMsg(mysqli_error());
                $object->setErrorCode(mysqli_errno());
                return 0;
            }
        }
    
    
        public static function fetchQueryResult(&$result, $type) {
            if ($type=="num")
                return mysqli_fetch_row($result);
            else
                return mysqli_fetch_assoc($result);
        }
    
        //muda o ponteiro do resultado $result para a posi��o $i
        public static function changeResultPoint(&$result,$i) {
            mysqli_data_seek($result,$i);
        }
    
        //transforma um result do bd pra array
        public static function result2Array($result) {
            $array = array();
            while ($dados = self::fetchQueryResult($result, "assoc"))
            array_push($array, $dados);
    
            return $array;
        }
    
        public static function scapeString(&$string) {
            return mysqli_real_escape_string($string, self::$DBLink);
        }
    
        public static function setDBHost($host) { self::$DBhost=$host; }
        public static function setDBName($name) { self::$DBName=$name; }
        public static function setDBUser($user) { self::$DBUser=$user; }
        public static function setDBPass($pass) { self::$DBPass=$pass; }
    
        public static function getErrorMsg()     { return self::$errorMsg;  }
        public static function getErrorCode()    { return self::$errorCode; }
    
    }

?>