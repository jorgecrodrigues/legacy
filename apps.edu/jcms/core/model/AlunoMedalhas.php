<?php

    class AlunoMedalhas extends ModelObject {
        
        private $alunoID;
        private $medalhasID=array();
        private $medalhasNomes=array();
        
        public function __construct($id=null) {
            if (!empty($id))
                $this->alunoID = $id;
        }
        
        public function getAlunoID() { return $this->alunoID; }
        public function getMedalhasID() { return $this->medalhasID; }
        public function getMedalhas() { return $this->medalhasNomes; }

        public function setAlunoID($id) { $this->alunoID=$id; }
        public function setMedalhasID($medalhas) { $this->medalhasID=$medalhas; }
        public function setMedalhas($medalhas) { $this->medalhasNomes=$medalhas; }
        
    }


?>