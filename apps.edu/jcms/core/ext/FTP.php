<?php

class FTP {

    private $ftpUserName;
    private $ftpUserPassword;
    private $ftpHost;
    private $connectionID;
    private $ftpError;

    public function __construct($host, $user, $pass) {
        $this->ftpHost=$host;
        $this->ftpUserName=$user;
        $this->ftpUserPassword=$pass;
    }

    public function ftpConnection() {
        $this->connectionID = ftp_connect($this->ftpHost);
        if ($this->connectionID !== false) {
            if (ftp_login($this->connectionID,$this->ftpUserName, $this->ftpUserPassword)) {
                return true;
            }
            else {
                $this->ftpError="N�o foi poss�vel efetuar login user: ".$this->ftpUserName;
                return false;
            }
        }
        else {
            $this->ftpError = "N�o foi poss�vel conectar ao host";
            return false;
        }
    }

    public function sendFile($file, $tmp_file) {
        if (ftp_put($this->connectionID, $file, $tmp_file, FTP_BINARY)) {
            return true;
        }
        else {
            return false;
        }
    }

    public function deleteFile($file) {
        if (ftp_delete($this->connectionID, $file)){
            return true;
        }
        else {
            return false;
        }
    }

    public function getFTPError() {
        return $this->ftpError;
    }

    public function createDir($dir) {
        if (ftp_mkdir($this->connectionID, $dir))
            return true;
        else
            return false;
    }

    public function getConnection() {
        return $this->connectionID;
    }

    public function rename($old_path, $new_path) {
        if (ftp_rename($this->connectionID,$old_path, $new_path))
            return true;
        else
            return false;
    }
    
    public function command($command) {
        ftp_site($this->connectionID,$command);
    }

    public function removeDir($dir) {
        if (ftp_rmdir( $this->connectionID, $dir))
            return true;
        else
            return false;
    }
    
    public function close() {
        ftp_close($this->connectionID);
    }
    

}

?>