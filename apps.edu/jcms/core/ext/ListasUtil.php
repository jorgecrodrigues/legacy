<?php

    class ListasUtil {
        
        private static $comboEquipes;
        
        public static function listaCategorias() {
            
            $array_categorias=array();
            
            $categoria=new Categoria();
            CategoriasDAO::setObject($categoria);
            CategoriasDAO::addSorter(array("nome","ASC"));
            CategoriasDAO::DBConnection();
            CategoriasDAO::listItems(0,0);            
            
            $rowIndex=0;
            $numRows=$categoria->getNumRows();
            
            while ($rowIndex<$numRows) {
                CategoriasDAO::fillObject();
                $array_categorias[$categoria->getCategoriaID()] = $categoria->getNome();
                $rowIndex++;
            }
            
            return $array_categorias;
            
        }
		
        public static function listaJogos() {
            
            $array_jogos=array();
            
            $jogo=new Jogo();
            JogosDAO::setObject($jogo);
            JogosDAO::addSorter(array("nome","ASC"));
            JogosDAO::DBConnection();
            JogosDAO::listItems(0,0);            
            
            $rowIndex=0;
            $numRows=$jogo->getNumRows();
            
            while ($rowIndex<$numRows) {
                JogosDAO::fillObject();
                $array_jogos[$jogo->getJogoID()] = $jogo->getNome();
                $rowIndex++;
            }
            
            return $array_jogos;
            
        }

    }


?>