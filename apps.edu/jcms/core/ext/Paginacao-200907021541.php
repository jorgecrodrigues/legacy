<?php

    class Paginacao {
        
        public $total_resultados;
        public $linhas_por_pagina;
        public $total_paginas;
        public $pagina_atual;
        public $limiar;
        public $primeira = false;
        public $anterior = true;
        public $proxima = true;
        public $ultima = false;
        public $max_paginas;
        public $base_url;
        public $query_string;
        private $paginacao="";
        
        public function paginar() {
            
            //numero de p�ginas necess�rias    
            $this->total_paginas = ceil($this->total_resultados/$this->linhas_por_pagina);
            
            $this->query_string = str_replace("&page=".$this->pagina_atual, "", $this->query_string);
        
            $this->paginacao .= "<a href='".$this->base_url."?".$this->query_string."&page=1' title='primeira'>&laquo;</a>&nbsp;";
            
            if ($this->total_paginas > 20) {
                
                //pagina atual ta na primeira metade                
                if ($this->pagina_atual < (floor($this->total_paginas/2))) {
                
                    $p_dif = $this->pagina_atual-$this->limiar;
                    $p_ini = ($p_dif < 1) ? 1 : ($p_dif+1);
                    $p_fim = ($p_dif < 1) ? (($this->pagina_atual+$this->limiar)+($p_dif*-1)) : ($this->pagina_atual+$this->limiar);
                    
                    for ($i=$p_ini;$i<=$p_fim;$i++) {
                        if ($this->pagina_atual==$i)
                            $this->paginacao .= "[".$i."]&nbsp;";
                        else
                            $this->paginacao .= "<a href='".$this->base_url."?".$this->query_string."&page=".$i."'>".$i."</a>&nbsp;";
                    }
                    $this->paginacao .= "... ";
                    for ($i=($this->total_paginas-9);$i<=$this->total_paginas;$i++) {
                        $this->paginacao .= "<a href='".$this->base_url."?".$this->query_string."&page=".$i."'>".$i."</a>&nbsp;";
                    }                        
            
                }
                else {
                    $p_soma = $this->pagina_atual+$this->limiar;
                    $p_ini = ($p_soma > $this->total_paginas) ? ($this->pagina_atual-$this->limiar)-($p_soma-$this->total_paginas)+1 : $this->pagina_atual-$this->limiar-1;
                    $p_fim = ($p_soma > $this->total_paginas) ? $this->total_paginas : $p_soma;
                    
                    for ($i=1;$i<=10;$i++) {
                        $this->paginacao .= "<a href='".$this->base_url."?".$this->query_string."&page=".$i."'>".$i."</a>&nbsp;";
                    }
                    $this->paginacao .= "... ";
                    for ($i=$p_ini;$i<=$p_fim;$i++) {
                        if ($this->pagina_atual==$i)
                            $this->paginacao .= "[".$i."]&nbsp;";
                        else
                            $this->paginacao .= "<a href='".$this->base_url."?".$this->query_string."&page=".$i."'>".$i."</a>&nbsp;";
                    }  
                    
                }
            
            }
            else {
                for ($i=1;$i<=$this->total_paginas;$i++) {
                    if ($this->pagina_atual==$i)
                        $this->paginacao .= "[".$i."]&nbsp;";
                    else
                        $this->paginacao .= "<a href='".$this->base_url."?".$this->query_string."&page=".$i."'>".$i."</a>&nbsp;";                            
                }
            }            
            $this->paginacao .= "<a href='".$this->base_url."?".$this->query_string."&page=".$this->total_paginas."' title='�ltima'>&raquo;</a>";
        }
        
        public function getPaginacao() { return $this->paginacao; }
        
        
    }


?>