<?php

class Mailer {
    
    private static $destinatarios=array();
    private static $dest_bcc=array();
    public static $assunto;
    public static $corpo;
    public static $error;
    
    public static $mailFrom;
    public static $mailFromName;
        
    public static function addDestinatarios($dest) {
        if (count($dest) > 0) {
            foreach ($dest as $d) {
                array_push(self::$destinatarios, $d);
            }
        }
    }
    
    public static function addDestBCC($dest) {
        if (count($dest) > 0) {
            foreach ($dest as $d) {
                array_push(self::$dest_bcc, $d);
            }
        }        
    }
    
    public static function clearDestinatarios() {
        self::$destinatarios=array();
    }
 
    public static function clearDestsBCC() {
        self::$dest_bcc=array();
    } 
    
    public static function sendMail($auth=false) {

        try {

            $mail             = new PHPMailer();
    
            if ($auth) {
                $mail->IsSMTP(); // telling the class to use SMTP
                $mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)            
                $mail->SMTPAuth = true;            
                $mail->Host       = EMAIL_SMTP_HOST; // SMTP server
                $mail->Port       = intval(EMAIL_SMTP_PORT);
                $mail->Username   = EMAIL_SMTP_LOGIN;
                $mail->Password   = EMAIL_SMTP_PASS;
            }
    
            $mail->From       = self::$mailFrom;
            $mail->FromName   = self::$mailFromName;
            $mail->Subject = self::$assunto;
            $mail->MsgHTML(self::$corpo);
                
            foreach (self::$destinatarios as $d) {
                if (is_array($d))
                    $mail->AddAddress($d[0],$d[1]);
                else
                    $mail->AddAddress($d);
            }
            
            if (count(self::$dest_bcc) > 0) {     
                foreach (self::$dest_bcc as $d) {
                    if (is_array($d))
                        $mail->AddBCC($d[0],$d[1]);
                    else
                        $mail->AddBCC($d);
                }
            }
            
            $mail->CharSet="iso-8859-1";
            $mail->SMTPDebug = false;
            $mail->do_debug = 0;            
            
            if ($mail->Send())
                return 1;
            else {            
                self::$error=$mail->ErrorInfo;
                return 0;                
            }
        
        }
        catch (phpmailerException $e) {
            //echo $e->errorMessage();
            self::$error=$e->errorMessage();
        } catch (Exception $e) {
            //echo $e->getMessage(); //Boring error messages from anything else!
            self::$error.="<br/>".$e->errorMessage();
        }        
        return 0;
    }
}

?>