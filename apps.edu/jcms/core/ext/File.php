<?php

    class File {
    
        private static $specialChars = array(array(" ","~","^","'","�","`","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�","�"),array("_","_","_","_","_","_","_","c","a","a","a","a","a","e","e","e","e","i","i","i","i","o","o","o","o","o","u","u","u","u","y","y"));        
        private static $imagePattern = "/(jpg|jpeg|gif|png|bmp)/";
    
        public static function removeSpecialChars($string) {        
            $string = str_ireplace(self::$specialChars[0],self::$specialChars[1],strtolower($string));
            $newString = "";
            for ($i=0;$i<strlen($string);$i++) {
                if (((ord($string[$i])==32) || (ord($string[$i])==45) || (ord($string[$i])==46) || (ord($string[$i])==95)) || (((ord($string[$i]) >= 48) && (ord($string[$i]) <= 57)) || ((ord($string[$i]) >= 65) && (ord($string[$i]) <= 90)) || ((ord($string[$i]) >= 97) && (ord($string[$i]) <=  122))))
                    $newString .= $string[$i];
            }
            return $newString;        
        
        }

        public static function isImage($file) {
            $ext = self::extension($file);
            if (preg_match(self::$imagePattern, $ext))
                return true;
            else
                return false;
        }
        
        public static function isPDF($file) {
            $ext = self::extension($file);
            if (ereg("pdf", $ext))
                return true;
            else
                return false;
        }        
        
        public static function isFlashFile($file) {
            $ext = self::extension($file);
            if (ereg("swf", $ext))
                return true;
            else
                return false;            
        }
        
        public static function extension($file_name) {
            if (!empty($file_name))
                return strtolower(substr(strrchr($file_name, chr(46)),1));
        }
        
        public static function put($file, $path_dir) {            
            if (!empty($file) && !empty($path_dir)) {
                if (copy($file, $path_dir))
                    return 1;
                else
                    return 0;
            }
            else
                return 0;
        }
        
        public function rename ($file, $new) {
            if (is_file($file)) {
                if (rename($file, $new))
                    return 1;
                else
                    return 0;
            }
            else
                return 0;
        }
        
        public static function remove($file) {
            if (is_file($file)) {
                if (unlink($file))
                    return 1;
                else
                    return 0;
            }
            else
                return 0;
        }
        
        public static function createDir($dir, $mode=0) {
            $mode = ($mode==0) ? 0777 : $mode;
            if (mkdir($dir, $mode))
                return 1;
            else
                return 2;
            
        }
        
    }


?>