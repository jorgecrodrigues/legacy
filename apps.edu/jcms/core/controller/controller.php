<?php

require_once("../../setup/autoload.php");

ob_start();

require_once("flowMap.php");

define("INDEX", 1);

$object = $_REQUEST['object'];
$action = $_REQUEST['action'];

$face = isset($_REQUEST['face']) ? $_REQUEST['face'] : null;

if (isset($flowMap[$face][$object][$action])) {
    
    if (($face=="backend") && ($action!="login")) {
        require('../../setup/auth.php');    
        if (!$authUser) die("Para executar esse script � necess�rio efetuar login!");
    }
    
    if (isset($flowMap[$face][$object][$action]['action'])) {
        if ($face=="backend")
            require_once("../../setup/auth.php");
        
        require_once(PATH_SYS.$flowMap[$face][$object][$action]['action']);
    }
    
    if (isset($flowMap[$face][$object][$action]['view'])) {
        
        if ($flowMap[$face][$object][$action]['view']=="AJAX") {
            header('Content-type: text/plain; charset=utf-8');
            echo json_encode($json_retorno);
        }
        else {
            if (is_array($flowMap[$face][$object][$action]['view'])) {
                if (isset($sucess) && ($sucess===true)) {
                    $viewURL = $flowMap[$face][$object][$action]['view']['sucess'];
                    unset($sucess);
                }
                else
                    $viewURL = $flowMap[$face][$object][$action]['view']['fail'];
            }
            else
                $viewURL = $flowMap[$face][$object][$action]['view'];
            
            $query_string = explode("&",$_SERVER['QUERY_STRING']);
            
            $new_query_string = "";
            
            if (count($query_string) > 2) {
                for ($i=3;$i<count($query_string);$i++) {
                    $new_query_string .= $query_string[$i]."&";
                }
            }
            
            if ($viewURL == "FOLLOW") {
                header("Location: ".$_SERVER['HTTP_REFERER']);
            }
            else {
                
                if (isset($key))
                    $viewURL = str_replace("%key",$key,$viewURL);
                
                $query_string = explode("&",$_SERVER['QUERY_STRING']);
                
                $new_query_string = "";
                
                if (count($query_string) > 2) {
                    for ($i=3;$i<count($query_string);$i++) {
                        $new_query_string .= $query_string[$i]."&";
                    }
                }
                
                $new_query_string = substr($new_query_string, 0, strlen($new_query_string)-1);            
                
                if (!empty($new_query_string)) {
                    if (strpos($viewURL, "?") !== false)                   
                        $viewURL .= "&".$new_query_string;
                    else
                        $viewURL .= "?".$new_query_string;
                }
                
                if ($face=="frontend") {
                    header("Location: ".DIR_PAGE.$viewURL);
                }
                else
                    header("Location: ".DIR_SYS.$viewURL);
            }
        }
        
    }
}
else
    print("Registro inexistente!");

ob_end_flush();

unset($flowMap);

?>