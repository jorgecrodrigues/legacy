<?php

//vetor com o mapa de fluxo do sistema

/* FLOWMAP do back-end */

$flowMap['backend'] = array();

$flowMap['backend']['aluno'] = array();
$flowMap['backend']['aluno']['cadastrar'] = array();
$flowMap['backend']['aluno']['cadastrar']['action'] = "/core/actions/alunos/cadastrar.php";
$flowMap['backend']['aluno']['cadastrar']['view'] = "/view/cadastro-aluno.php";
$flowMap['backend']['aluno']['atualizar'] = array();
$flowMap['backend']['aluno']['atualizar']['action'] = "/core/actions/alunos/atualizar.php";
$flowMap['backend']['aluno']['atualizar']['view'] = "/view/atualiza-aluno.php";
$flowMap['backend']['aluno']['deletar'] = array();
$flowMap['backend']['aluno']['deletar']['action'] = "/core/actions/alunos/deletar.php";
$flowMap['backend']['aluno']['deletar']['view'] = "/view/lista-alunos.php";

$flowMap['backend']['configuracoes'] = array();
$flowMap['backend']['configuracoes']['salvar'] = array();
$flowMap['backend']['configuracoes']['salvar']['action'] = "/core/actions/configuracoes/salvar.php";
$flowMap['backend']['configuracoes']['salvar']['view'] = "/view/index.php";

$flowMap['backend']['jogo'] = array();
$flowMap['backend']['jogo']['cadastrar'] = array();
$flowMap['backend']['jogo']['cadastrar']['action'] = "/core/actions/jogos/cadastrar.php";
$flowMap['backend']['jogo']['cadastrar']['view'] = "/view/cadastro-jogo.php";
$flowMap['backend']['jogo']['atualizar'] = array();
$flowMap['backend']['jogo']['atualizar']['action'] = "/core/actions/jogos/atualizar.php";
$flowMap['backend']['jogo']['atualizar']['view'] = "/view/atualiza-jogo.php";

$flowMap['backend']['institucional'] = array();
$flowMap['backend']['institucional']['atualizar'] = array();
$flowMap['backend']['institucional']['atualizar']['action'] = "/core/actions/institucionais/atualizar.php";
$flowMap['backend']['institucional']['atualizar']['view'] = "/view/atualiza-institucional.php";

$flowMap['backend']['medalha'] = array();
$flowMap['backend']['medalha']['cadastrar'] = array();
$flowMap['backend']['medalha']['cadastrar']['action'] = "/core/actions/medalhas/cadastrar.php";
$flowMap['backend']['medalha']['cadastrar']['view'] = "/view/cadastro-medalha.php";
$flowMap['backend']['medalha']['atualizar'] = array();
$flowMap['backend']['medalha']['atualizar']['action'] = "/core/actions/medalhas/atualizar.php";
$flowMap['backend']['medalha']['atualizar']['view'] = "/view/atualiza-medalha.php";
$flowMap['backend']['medalha']['deletar'] = array();
$flowMap['backend']['medalha']['deletar']['action'] = "/core/actions/medalhas/deletar.php";
$flowMap['backend']['medalha']['deletar']['view'] = "/view/lista-medalhas.php";

$flowMap['backend']['usuario'] = array();
$flowMap['backend']['usuario']['login'] = array();
$flowMap['backend']['usuario']['login']['action'] = "/core/actions/usuarios/login.php";
$flowMap['backend']['usuario']['login']['view']['sucess'] = "/view/index.php";
$flowMap['backend']['usuario']['login']['view']['fail'] = "/view/login.php";
$flowMap['backend']['usuario']['logoff'] = array();
$flowMap['backend']['usuario']['logoff']['action'] = "/core/actions/usuarios/logoff.php";
$flowMap['backend']['usuario']['logoff']['view'] = "/view/login.php";

?>