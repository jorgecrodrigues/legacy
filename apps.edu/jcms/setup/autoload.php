<?php

    error_reporting(E_ALL);
    //error_reporting(E_ALL ^ E_NOTICE ^ ~E_STRICT ^ E_DEPRECATED);
	//error_reporting(E_ALL);
    session_start();
    date_default_timezone_set("America/Cuiaba");
    
    //defini��es de diret�rio
    define('DIR_PAGE',   '/apps.edu');
    define('DIR_SYS',    '/apps.edu/jcms');
    define('PATH_SYS',   $_SERVER['DOCUMENT_ROOT'] . DIR_SYS);
    define('PATH_SITE',  '/var/www/html'.DIR_PAGE);
    define('PATH_ROOT',  '/var/www/html'.DIR_SYS);
    
    define("REGEX_EMAIL","/^([0-9a-zA-Z]+([_.-]?[0-9a-zA-Z]+)*@[0-9a-zA-Z]+[0-9,a-z,A-Z,.,-]+(.){1}[a-zA-Z]{2,4})+$/");
    define("REGEX_USERNAME","/^[a-zA-Z0-9_-]{3,12}$/");
    define("REGEX_DATE","&^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$&");

    function __autoLoad($name) {
        $dir_base=PATH_SYS . "/core/";
        if (preg_match("/.*DAO\.php/",$name.".php")) {
            if (is_file($dir_base . "model/DAO/" . $name . ".php"))
                require_once($dir_base . "model/DAO/" . $name . ".php");
            else
                print("<br/>Classe ".$name." n�o existe!");
        }
        else if (preg_match("/.*ACT\.php/",$name.".php")) {
            if (file_exists($dir_base . "actions/" . $name . ".php"))
                require_once($dir_base . "actions/" . $name . ".php");
            else
                print("<br/>Classe ".$name." n�o existe!");             
        }
        else if (preg_match("/.*VIEW\.php/",$name.".php")) {
            if (file_exists(PATH_SYS . "/view/classes/" . $name . ".php"))
                require_once(PATH_SYS . "/view/classes/" . $name . ".php");
            else
                print("<br/>Classe ".$name." n�o existe!");             
        }        
        else {
            if (file_exists($dir_base . "model/" . $name . ".php"))
                require_once($dir_base . "model/" . $name . ".php");
            else if (file_exists($dir_base . "ext/" . $name . ".php"))
                require_once($dir_base . "ext/" . $name . ".php");
            else
                print("<br/>Classe ".$name." n�o existe!");
        }
    }
	
    //defini��es de hosts, login e senha
    define("DB_HOST","localhost");
    define("DB_NAME","gic_appsedu");
    define("DB_USER","gic_appsedu");
    define("DB_PASS","cbie2015");
    
    define("EMAIL_SENDER",     "");
    define("EMAIL_SMTP_HOST",  "");
    define("EMAIL_SMTP_PORT",  "");
    define("EMAIL_SMTP_LOGIN", "");
    define("EMAIL_SMTP_PASS",  "");
    
    $ftpHost="";
    $ftpUserName="";
    $ftpUserPassword="";

?>
