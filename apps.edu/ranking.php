<?php
	require_once('jcms/setup/autoload.php');
	
	$sigla = $_REQUEST['jogo'];
	
	$objJogo = new Jogo();
	$objJogo->setSigla($sigla);
	JogosDAO::setObject($objJogo);
	JogosDAO::DBConnection();
	JogosDAO::getObjectDBData();
	
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"> 
        <meta charset="ISO-8859-1">
        <title>Gamifica��o - Profa. Karen</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <style>
            div.container-ranking {
                display: table;
                width: 100%;
            }

            div.container-ranking div.ranking {
                display: table-cell;
				padding-right: 15px;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <h4>
                <i class="glyphicon"><img src="http://gameinclass.ic.ufmt.br/apps.edu/jcms/view/images/chess.png" /></i>
                &nbsp;<?= $objJogo->getNome() ?>
            </h4>
            <div class="container">
                <?php
                    $rankings_exibidos = $objJogo->getRanking();
                ?>
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Ranking do Jogo</h4></div>
					<div class="panel-body">
						<?php
							if ($rankings_exibidos == "0000") {
								?>
						<div class="panel-heading"><h4>Nenhum Ranking a ser mostrado para esse Jogo</h4></div>
								<?php
							}
							else {
						?>
						<div class="container-ranking">
							<?php
								if ($rankings_exibidos{0} == '1') {
							?>
							<div id="ranking-pontos-individual" class="ranking">
								<h3>Ranking Individual por Pontos</h3>
								<?php
								$aluno = new Aluno();
								AlunosDAO::setObject($aluno);
								AlunosDAO::addFilter(array("jogo_id", "=", $objJogo->getJogoID()));
								AlunosDAO::addSorter(array("total_pontos", "DESC"));
								AlunosDAO::listItems(0, 0);
								?>
								<table class="table table-striped">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Jogador</th>
											<th>Pontos</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$rowIndex = 0;
										$numRows = $aluno->getNumRows();

										while ($rowIndex < $numRows) {
											AlunosDAO::fillObject();
											?>
											<tr <?= ($rowIndex == 0) ? "style='font-weight: bold'" : null ?>>
												<td><?= $rowIndex + 1 ?>�</td>
												<td><a href="/apps.edu/aluno/<?= $aluno->getAlias() ?>"><?= $aluno->getAlias() ?></a></td>
												<td><?= $aluno->getTotalPontos() ?></td>
											</tr>
											<?php
											$rowIndex++;
										}
										?>  
									</tbody>
								</table>                                
							</div>
							<?php
								}
								if ($rankings_exibidos{1} == '1') {
							?>
							<div id="ranking-pontos-grupos" class="ranking">
								<h3>Ranking por Pontos por Grupo</h3>
								<table class="table table-striped">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Grupo</th>
											<th>Jogadores</th>
											<th>Total de Pontos</th>
											<th>M�dia de Pontos</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$aluno = new Aluno();
										AlunosDAO::setObject($aluno);
										$array_grupos = AlunosDAO::rankingPtsByGroups($objJogo->getJogoID());
										foreach ($array_grupos as $k => $g) {
											?>
											<tr <?= ($k == 0) ? "style='font-weight: bold'" : null ?>>
												<td><?= $k + 1 ?>�</td>
												<td><?= $g['grupo'] ?></td>
												<td><?= $g['membros'] ?></td>
												<td><?= $g['pontos'] ?></td>
												<td><?= number_format($g['media'], 2, '.', '') ?></td>
											</tr>
											<?php
										}
										?>  
									</tbody>
								</table>                                
							</div>
							<?php
								}
							?>
						</div>
						<div class="container-ranking">
							<?php
								if ($rankings_exibidos{2} == '1') {
							?>
							<div id="ranking-individual-medalhas" class="ranking">
								<h3>Ranking Individual por Medalhas</h3>
								<?php
									$aluno = new Aluno();
									AlunosDAO::setObject($aluno);
									AlunosDAO::clearFilter();
									AlunosDAO::clearSorter();
									AlunosDAO::rankingByMedalhas($objJogo->getJogoID());
								?>
								<table class="table table-striped">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Jogador</th>
											<th>Medalhas</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$rowIndex = 0;
										$numRows = $aluno->getNumRows();

										while ($rowIndex < $numRows) {
											AlunosDAO::fillObject();
											?>
											<tr <?= ($rowIndex == 0) ? "style='font-weight: bold'" : null ?>>
												<td><?= $rowIndex + 1 ?>�</td>
												<td><a href="/apps.edu/aluno/<?= $aluno->getAlias() ?>"><?= $aluno->getAlias() ?></a></td>
												<td><?= $aluno->getTotalMedalhas() ?></td>
											</tr>
											<?php
											$rowIndex++;
										}
										?>  
									</tbody>
								</table>                                
							</div>
							<?php
								}
								if ($rankings_exibidos{3} == '1') {
							?>
							<div id="ranking-medalhas-grupos" class="ranking">
								<h3>Ranking por Medalhas por Grupos</h3>
								<table class="table table-striped">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Grupo</th>
											<th>Jogadores</th>
											<th>Total de Medalhas</th>
											<th>M�dia de Medalhas</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$aluno = new Aluno();
										AlunosDAO::setObject($aluno);
										$array_grupos = AlunosDAO::rankingMedalhasByGroups($objJogo->getJogoID());
										foreach ($array_grupos as $k => $g) {
											?>
											<tr <?= ($k == 0) ? "style='font-weight: bold'" : null ?>>
												<td><?= $k + 1 ?>�</td>
												<td><?= $g['grupo'] ?></td>
												<td><?= $g['membros'] ?></td>
												<td><?= $g['medalhas_total'] ?></td>
												<td><?= number_format($g['media'], 2, '.', '') ?></td>
											</tr>
											<?php
										}
										?>  
									</tbody>
								</table>                                
							</div>
							<?php
								}
							?>
						</div>
						<?php
							}
						?>
					</div>
				</div>
            </div>
        </div>
        <script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type='text/javascript' src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <script type='text/javascript'>
            $(document).ready(function () {
            });
        </script>        
    </body>
</html>